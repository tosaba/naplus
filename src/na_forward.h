#ifndef na_forward_H
#define na_forward_H

// --- system include ---------------------------------------------------------
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>
using namespace std;

// --- local include ----------------------------------------------------------
#ifdef MATLAB
#include "libmatlab_fwrd.h"
#endif
#ifdef LaMEM
#include "petsc.h"
#endif
#include "naplus_utils.h"
#include "naplus_globalvars.h"

// --- declaration of functions -----------------------------------------------
void forward(int nd,NaCurrentResult &cr,MPI_Comm fwrd_comm,int opt_cpu,int fwrd_group_id,int cntModels);
void forward_LaMEM(int nd,double *current_model,double *misfit,MPI_Comm fwrd_comm,int opt_cpu,int fwrd_group_id,int cntModels,int IorM,double &diff_t);
void LaMEMInterface_Initialize(MPI_Comm fwrd_comm, double *current_model,int cntModels,int fwrd_group_id,time_t start_t);
void LaMEMInterface_Finalize(MPI_Comm fwrd_comm,double misfit, int cntModels,double diff_t);
void print_user_info(int opt_rank,int var);
#ifdef __cplusplus
extern "C" {
#endif
#ifdef LaMEM
	PetscErrorCode LaMEMLib(PetscScalar *LaMEM_OutputParameters,PetscInt *mpi_group_id);
#endif
#ifdef __cplusplus
}
#endif
void Rosenbrock2D(int nd, double *current_model, double *misfit);
void Himmelsblau2D(int nd, double *current_model, double *misfit);
void MultipleLocalMinima2D(int nd, double *current_model, double *misfit);
void RastriginND(int nd, double *current_model, double *misfit);
void RosenbrockND(int nd, double *current_model, double *misfit);
void EffViscosity(double *current_model, double *mu_eff);
void forward_matlab(int nd, double *current_model, double *misfit,MPI_Comm fwrd_comm,int opt_cpu,int fwrd_group_id,int cntModels,int IorM,double &diff_t);
void FwrdModInterface_Initialize(MPI_Comm fwrd_comm, int nd, double *current_model,int cntModels,int fwrd_group_id,time_t start_t);
void FwrdModInterface_Finalize(MPI_Comm fwrd_comm,double misfit, int cntModels,double diff_t);
//--- templates ---------------------------------------------------------------
template <class T>
void print_array_formated(T *array, int length)
{
	for (int n=0; n<length; n++)
		cout << array[n] << "\t\t";
	cout << endl;
}
//-----------------------------------------------------------------------------
#endif

!    -----------------------------------------------
!    This is the fortran equivalent to na_types.h
!    
!    -----------------------------------------------
     type, bind(c) :: nausercontext_f
        type(c_ptr)    :: ranges_in
        type(c_ptr)    :: scales_in
        type(c_ptr)    :: prngstream
        integer(c_int) :: nd
        integer(c_int) :: nsamplei
        integer(c_int) :: nsamplei_file
        integer(c_int) :: nsample
        integer(c_int) :: restart
        integer(c_int) :: manadvance
        integer(c_int) :: ncells
        integer(c_int) :: itmax
        integer(c_int) :: nummodels
        integer(c_int) :: restartna
        integer(c_int) :: nainit_idnext
        integer(c_int) :: opt_rank,opt_size
        integer(c_int) :: initsamp
        integer(c_int) :: numwbuff
        integer(c_int) :: numdisc
     end type

     type(nausercontext_f) user

    interface
      subroutine prng_unfrand01(stream,rank,rnd) bind (c)
        use iso_c_binding
        type(c_ptr)   ,value :: stream
        integer(c_int),value :: rank
        real(c_double)       :: rnd
      end subroutine prng_unfrand01
    end interface

    interface
      subroutine prng_unfrandab(stream,rank,a,b,rnd) bind (c)
        use iso_c_binding
        type(c_ptr)   ,value :: stream
        integer(c_int),value :: rank
        real(c_double),value :: a,b
        real(c_double)       :: rnd
      end subroutine prng_unfrandab
    end interface

    interface
      subroutine prng_unfintrandab(stream,rank,ia,ib,irnd) bind (c)
        use iso_c_binding
        type(c_ptr)   ,value :: stream
        integer(c_int),value :: rank
        integer(c_int),value :: ia,ib
        integer(c_int)       :: irnd
      end subroutine prng_unfintrandab
    end interface

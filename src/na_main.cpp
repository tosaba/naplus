
#include "na_main.h"

// ----------------------------------------------------------------------------
void checkList(list <SendBuffer*> &msg_list,MPI_Comm comm,int ierr){

    list <SendBuffer*> :: iterator     it;
    it = msg_list.begin();
    while(it != msg_list.end()){
        SendBuffer *msg = *it;
        if(msg->test_received(comm,ierr))
        {
            delete msg;
            it = msg_list.erase(it);
        }
        else it++;
    }
    return;
}
// ----------------------------------------------------------------------------
void checkListSizeErrors(MPI_Comm comm,NaUserContext &user,list <SendBuffer*> &msg_list){
    if(msg_list.size()){
        printf("[NAplus Error: %d] Remaining messages in msg_list\n",user.opt_rank);
        check_err(comm,15);
    }
    return;
}
//------------------------------------------------------------------------------
void calcModel(MPI_Comm fwrd_comm,NaUserContext &user,NaCurrentResult &cr,double *all_models,int mID)
{
    // Get current model
    for(int k=0;k<user.nd;k++)
        cr.sca_model[k] = all_models[user.nd*mID+k];
    // Transform current_model to raw
    FSUB(transform2raw)(&user,cr.sca_model,cr.current_model);
    cr.current_misfit = 0.0;
    // Forward modeling
    forward(user.nd, cr,fwrd_comm,0,user.opt_rank,mID);
    cr.cntModels_own++;
    // Write models to disk
    write_models(user,cr,(double)mID,(double)(cr.cntModels_own),user.opt_rank,"models_own_");

    return;
}
//------------------------------------------------------------------------------
bool sendModel(MPI_Comm opt_comm,NaUserContext &user,NaWriteBuffer &wbuff,NaCurrentResult &cr,double *all_models,double *all_misfits,list <SendBuffer*> &msg_list,int &NSend_msg)
{
    int ierr=0;

    for(int i=0;i<user.opt_size;i++)
    {
        if(i != user.opt_rank)
        {
            SendBuffer *msg = new SendBuffer;
            msg->create_FinishedModel(user.nd,cr.cntModels_own,cr.sca_model,cr.current_misfit,cr.diff_t);
            msg->send(opt_comm,i,cr.tag,ierr);
            msg_list.push_back(msg);
            NSend_msg++;
        }
        else
        {
            // Insert misfit/model to own misfit/model history
            if (cr.cntModels<user.nummodels)
            {
                all_misfits[cr.cntModels] = cr.current_misfit;
                for(int k=0;k<user.nd;k++)
                    all_models[user.nd*cr.cntModels+k] = cr.sca_model[k];
            }
            //write_models(user,cr,(double)(cr.cntModels),(double)(cr.cntModels_own),user.opt_rank,"models_all_");
            //buffwrite_models(user,wbuff,cr,(double)(cr.cntModels),(double)(cr.cntModels_own),user.opt_rank,"models_all_");
            cr.cntModels++;
        }
    }

    //
    if(cr.InitOrMain == 0 && cr.cntModels==user.nsamplei) return(true);
    else if(cr.InitOrMain == 1 && cr.cntModels==user.nummodels) return(true);
    else return(false);
}
//------------------------------------------------------------------------------
bool recvModels(MPI_Comm opt_comm,NaUserContext &user,NaWriteBuffer &wbuff,NaCurrentResult &cr,double *all_models,double *all_misfits,int &NRecv_msg,int &NRecv)
{
    int ierr=0,msg_pending;
    MPI_Status status;

    NRecv = 0;
    ierr = MPI_Iprobe(MPI_ANY_SOURCE, cr.tag, opt_comm, &msg_pending, &status);check_err(opt_comm,ierr);
    while(msg_pending)
    {
        // Receive
        ierr = MPI_Recv(cr.recv_buffer,cr.recv_buffer_size, MPI_DOUBLE, status.MPI_SOURCE, status.MPI_TAG, opt_comm, &status);check_err(opt_comm,ierr);
        check_err(opt_comm,status.MPI_ERROR);
        NRecv_msg++;
        NRecv++;

        // Insert misfit/model to own misfit/model history
        if (cr.cntModels<user.nummodels)
        {
            all_misfits[cr.cntModels] = cr.recv_buffer[user.nd];
            for(int k=0;k<user.nd;k++)
                all_models[user.nd*cr.cntModels + k] = cr.recv_buffer[k];
        }

        // Write model+ misfit to file
        for(int k=0;k<user.nd;k++)
            cr.current_model[k]=cr.recv_buffer[k];
        cr.current_misfit = cr.recv_buffer[user.nd];
        cr.diff_t         = cr.recv_buffer[user.nd+2];

        FSUB(transform2raw)(&user,cr.current_model,cr.current_model);
        //write_models(user,cr,(double)(cr.cntModels),cr.recv_buffer[user.nd+1],status.MPI_SOURCE,"models_all_");
        //buffwrite_models(user,wbuff,cr,(double)(cr.cntModels),cr.recv_buffer[user.nd+1],status.MPI_SOURCE,"models_all_");

        // Update model counter
        cr.cntModels++;

        // Main optimization: stop immediately because all_models are complete: nummodels
        if(cr.InitOrMain == 1 && cr.cntModels==user.nummodels) return(true);

        // Check for other messages
        ierr = MPI_Iprobe(MPI_ANY_SOURCE, cr.tag, opt_comm, &msg_pending, &status);check_err(opt_comm,ierr);
    }
    // Initial optimization: stop: nsamplei or more models
    if(cr.InitOrMain == 0 && cr.cntModels>=user.nsamplei)  return(true);
    else return(false);
}
//-----------------------------------------------------------------------------
void recvRemainingMsg(MPI_Comm opt_comm,int tag,double *recv_buffer,int recv_buffer_size,int &NRecv_msg){
    int msg_pending;
    int ierr;

    ierr = MPI_Iprobe(MPI_ANY_SOURCE, tag, opt_comm, &msg_pending, MPI_STATUS_IGNORE);check_err(opt_comm,ierr);
    while(msg_pending)
    {
        ierr = MPI_Recv(recv_buffer,recv_buffer_size, MPI_DOUBLE, MPI_ANY_SOURCE,tag, opt_comm, MPI_STATUS_IGNORE);check_err(opt_comm,ierr);
        NRecv_msg++;
        ierr = MPI_Iprobe(MPI_ANY_SOURCE, tag, opt_comm, &msg_pending, MPI_STATUS_IGNORE);check_err(opt_comm,ierr);
    }
    return;
}
//-----------------------------------------------------------------------------
void checkCommErrors(MPI_Comm opt_comm,NaUserContext &user,int NSend_msg,int NRecv_msg)
{
    int ierr;
    int TotNSend_msg=0,TotNRecv_msg=0;

    // Get number of sent/received messages on root proc
    ierr = MPI_Reduce(&NSend_msg, &TotNSend_msg, 1, MPI_INT, MPI_SUM, 0, opt_comm);check_err(opt_comm,ierr);
    ierr = MPI_Reduce(&NRecv_msg, &TotNRecv_msg, 1, MPI_INT, MPI_SUM, 0, opt_comm);check_err(opt_comm,ierr);
    ierr = MPI_Barrier(opt_comm);check_err(opt_comm,ierr);

    // Error checking
    if(!user.opt_rank && (TotNSend_msg != TotNRecv_msg)){
        printf("[NAplus Error: all] Number of received messages (%d) !=  number of sent messages (%d) \n",TotNRecv_msg,TotNSend_msg);
        check_err(opt_comm,15);
    }
    if(!user.opt_rank) printf("[NAPlus:  all] Number of sent/received messages: %d\n",TotNSend_msg);
    return;
}
//-----------------------------------------------------------------------------
void cr_initialize(NaUserContext &user,NaCurrentResult &cr)
{
    cr.tag              = 256;
    cr.cntModels        = 0;
    cr.cntModels_own    = 0;
    cr.recv_buffer_size = user.nd+3;

    cr.current_model = new double[user.nd];
    cr.sca_model     = new double[user.nd];
    cr.recv_buffer   = new double[cr.recv_buffer_size];

    return;
}
//-----------------------------------------------------------------------------
void cr_finalize(NaCurrentResult &cr)
{
    delete [] cr.current_model;
    delete [] cr.sca_model;
    delete [] cr.recv_buffer;
    return;
}
//-----------------------------------------------------------------------------
void na(MPI_Comm opt_comm,MPI_Comm fwrd_comm)
{
    NaUserContext                  user;
    NaMfitStatistics               stats;
    NaCurrentResult                cr;
    NaWriteBuffer                  wbuff;
    list <SendBuffer*>             msg_list;
    int                            NSend_msg=0,NRecv_msg=0;
    int                            ierr,na_it,NRecv,iitmp,iitmp2;
    double                        *xcur;
    double                        *all_misfits,*all_models;
    double                        *ranget;
    double                        *dlist;
    double                        *discrange;
    int                            intrnd;
    double                         current_model;
    double                         t[10];

    // --- Mpi communication --------------------------------------------------
    ierr     = MPI_Comm_set_errhandler(opt_comm, MPI_ERRORS_RETURN);check_err(opt_comm,ierr);
    ierr     = MPI_Comm_rank(opt_comm, &user.opt_rank);check_err(opt_comm,ierr);
    ierr     = MPI_Comm_size(opt_comm, &user.opt_size);check_err(opt_comm,ierr);


    // --- Initialize ---------------------------------------------------------
    // Reading the input file
    UserContext_initialize(user);
    
    // Initialize structure of write buffer
    wbuff_initialize(user,wbuff);
    
    // Initialize structure of current result
    cr_initialize(user,cr);

    // Allocate arrays
    all_models    = new double[user.nd * user.nummodels]; // This way of ordering the 2D array is consistent with the 2D-accessing of the array in fortran subroutines
	//init_memory(all_models,user.nd * user.nummodels);
    all_misfits   = new double[user.nummodels];
    xcur          = new double[user.nd];
    ranget        = make_matrix(2,user.nd);
    dlist         = new double[user.nummodels];
    

    // Initialize NA routines
    //FSUB(na_initialize)(&user,ranget,all_misfits,all_models,xcur);
    na_initialize(user, ranget, all_misfits, all_models, xcur);

    // Read models from binary files
    if(user.restart) read_models(user,cr,all_models,all_misfits,"models_restart.bin");

    // Advance sub-stream if required
    PRNGAdvanceState(user);

    // Initialize statistics
    stats_initialize(user,stats);

    // Write header of output files
    if(!user.opt_rank) printf("[NAPlus:  all] Write header of output files\n");
    write_header(user,"models_own_");
    //write_header(user,"models_all_");


    // === Initial optimization ===============================================

    // Print user info
    MPI_Barrier(opt_comm);
    print_user_info(user.opt_rank,1);

    // Initial optimization loop as long as each process has less models than user.nsamplei
    if(user.nsamplei_file < user.nsamplei)
    {
        // constants
        cr.InitOrMain = 0;

        // Allocate discrete parameter vector
        discrange = new double[user.numdisc];

        // Return a normalized discrete parameter vector
        DiscParamRange(user,discrange);

        // Compute and receive models until the required number of models is reached
        do 
        {   

        	// (0) Take model parameters from look-up table
            for(int k=0;k<user.nd;k++)
            {
            	if(user.initsamp==1)
            	{
            		// return a random value on a discrete grid
            		PRNGUnfIntRandAB(user.prngstream, user.opt_rank, 0, user.numdisc-1, &intrnd);
            		current_model = (1.0 - discrange[intrnd])*ranget(0,k) + discrange[intrnd]*ranget(1,k);
            	}
            	else
            	{
            		// return a random value
            		PRNGUnfRandAB(user.prngstream, user.opt_rank, ranget(0,k), ranget(1,k), &current_model);
               	}
            	//all_models[user.nd*cr.cntModels+k] = all_models_init[user.opt_rank*(user.InitBlockSize*user.nd)+(cr.cntModels_own*user.nd)+k];
            	//if(user.initsamp==2)
            	//{
            	//            GetQuasiRandomSequence(user,all_models_init,ranget);
            	//}

            	all_models[user.nd*cr.cntModels+k] = current_model;
            }
            
            // (1) Get sample /forward model
            calcModel(fwrd_comm,user,cr,all_models,cr.cntModels);

            // (2) Send msg to all processes but self
            if(sendModel(opt_comm,user,wbuff,cr,all_models,all_misfits,msg_list,NSend_msg)) break;


            // (3) Receive all pending messages
            if(recvModels(opt_comm,user,wbuff,cr,all_models,all_misfits,NRecv_msg,NRecv)) break;

            // (4) Check msg_list and delete received messages
            checkList(msg_list,opt_comm,ierr);
            std::cout << "[NAplus debug: " << user.opt_rank << "] ListSize: " << msg_list.size() << "Sent: " << NSend_msg << "Recvd: " << NRecv_msg << " \n";
            
            if (cr.cntModels % 100 == 0) printf("[NAplus: %4d] %7d NRecv %7d \n",user.opt_rank,cr.cntModels,NRecv);
//            if ((time(NULL)%10)==0) std::cout << "Rank: " << user.opt_rank << "Listsize: " << msg_list.size() << "Sent: " << NSend_msg << "Recvd: " << NRecv_msg << " \n";

        } while(cr.cntModels<user.nsamplei);

        // Free allocated memory
        delete [] discrange;
    }

    // Evaluate misfits (orig. implem.: ntot=0,ns=nsamplei,na_it=1,ncells=nsamplei
    na_misfit(user,stats,all_misfits,0,cr.cntModels,1,cr.cntModels);

    // === End of initial optimization ========================================




    // === Main optimization ==================================================

    printf("[NAplus: %4d] Main optimisation \n",user.opt_rank);

    // Update iteration & model counter
    na_it         = 2;
    cr.InitOrMain = 1;

    do
    {
        // (0) Sampling
        t[0] = MPI_Wtime();
        FSUB(na_sample)(&user,all_models,&cr.cntModels,&user.nsample,all_misfits,stats.ff_mfit_ord,ranget,xcur,dlist);
        t[1] = MPI_Wtime();

        // (1) Get sample /forward model
        calcModel(fwrd_comm,user,cr,all_models,cr.cntModels);
        t[2] = MPI_Wtime();

        // (2) Send msg to all processes but self
        if(sendModel(opt_comm,user,wbuff,cr,all_models,all_misfits,msg_list,NSend_msg)) break;

        // (3) Receive all pending messages
        if(recvModels(opt_comm,user,wbuff,cr,all_models,all_misfits,NRecv_msg,NRecv)) break;

        // (4) Check msg_list and delete received messages
        checkList(msg_list,opt_comm,ierr);
        t[3] = MPI_Wtime();
//            if ((time(NULL)%10)==0) std::cout << "Rank: " << user.opt_rank << "Listsize: " << msg_list.size() << "Sent: " << NSend_msg << "Recvd: " << NRecv_msg << " \n";
        // (5) Evaluate misfits
        na_misfit(user,stats,all_misfits,(cr.cntModels - (NRecv+1)),(NRecv+1),na_it,user.ncells);
        t[4] = MPI_Wtime();

        iitmp  = (int) (cr.cntModels/user.nsample);
        iitmp2 = (int) ((cr.cntModels - (NRecv+1))/user.nsample);
        if(iitmp>iitmp2){
            na_it = na_it+(iitmp-iitmp2);
        }
        if (cr.cntModels % 100 == 0) printf("[NAplus: %4d] %7d cnt | %4.2f %4.2f %4.2f %4.2f %4.2f sec | %7d nrecv \n",user.opt_rank,cr.cntModels,t[4]-t[0],t[1]-t[0],t[2]-t[1],t[3]-t[2],t[4]-t[3],NRecv);

    } while (cr.cntModels < user.nummodels);
    print_user_info(user.opt_rank,2);

    // === End of main optimization ===========================================



    // --- Ensure that all sending operations have been terminated ------------
    ierr = MPI_Barrier(opt_comm);check_err(opt_comm,ierr);

    // --- Receive remaining messages -----------------------------------------
    recvRemainingMsg(opt_comm,cr.tag,cr.recv_buffer,cr.recv_buffer_size,NRecv_msg);
    checkList(msg_list,opt_comm,ierr);

    // --- Error checking -----------------------------------------------------
    checkCommErrors(opt_comm,user,NSend_msg,NRecv_msg);
    checkListSizeErrors(opt_comm,user,msg_list);

    // --- Finalize -----------------------------------------------------------

    // finalize user context
    UserContext_finalize(user);

    // finalize structure of write buffer
    wbuff_finalize(wbuff);

    // finalize current results
    cr_finalize(cr);

    // finalize statistics
    stats_finalize(stats);

    // Deallocate arrays
    delete [] all_models;
    delete [] all_misfits;
    delete [] xcur;
    delete [] dlist;
    delete [] ranget;

    return;
}

#ifndef na_main_H
#define na_main_H

// --- system include ---------------------------------------------------------
#include <mpi.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <list>
#include <algorithm>
using namespace std;

// --- local include ----------------------------------------------------------
#include "na_types.h"
#include "na_user.h"
#include "na_forward.h"

// === class definition =======================================================
class SendBuffer
{
 public:
    SendBuffer();                    // default constructor
    ~SendBuffer();                   // destructor

    void create_FinishedModel(int model_dim, int modelID, double *current_model, double current_misfit,double clctime);
    bool test_received(MPI_Comm comm, int err);
    void send(MPI_Comm comm, int dest_rank, int tag, int err);

 private:
    int          bsize;
    double      *buffer;
    MPI_Request  request;

};

// === class implementation ===================================================
// ----------------------------------------------------------------------------
// default constructor
SendBuffer::SendBuffer()
{
    bsize          = 0;
    buffer         = NULL;
    request        = MPI_REQUEST_NULL;
}
// ----------------------------------------------------------------------------
// default destructor
SendBuffer::~SendBuffer()
{
    if(buffer)    { delete [] buffer; buffer = NULL; }
}
// ----------------------------------------------------------------------------
void SendBuffer::create_FinishedModel(int model_dim, int modelID, double *current_model, double current_misfit,double clctime)
{
    bsize  = model_dim + 3;
    buffer = new double[bsize];
    for(int i(0); i < model_dim; i++) buffer[i] = current_model[i];
    buffer[model_dim]   = current_misfit;
    buffer[model_dim+1] = (double) modelID;
    buffer[model_dim+2] = clctime;
}
// ----------------------------------------------------------------------------
bool SendBuffer::test_received(MPI_Comm comm,int err)
{
    int received;
    // check whether messages were received
    err = MPI_Test(&request, &received, MPI_STATUS_IGNORE); ::check_err(comm,err);
    return (bool) received;
}
// ----------------------------------------------------------------------------
void SendBuffer::send(MPI_Comm comm, int dest_rank, int tag, int err)
{
    err = MPI_Isend(buffer, bsize, MPI_DOUBLE, dest_rank, tag, comm, &request); ::check_err(comm,err);
}
// ----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
template <class T>
void init_memory(T *& a, const int n = 1)
{
	// clear memory
	memset(a, 0, sizeof(T)*n);
};


// === Declaration of fortran sub functions ===================================
#ifdef __cplusplus
extern "C" {
#endif
    void FSUB(print_matrix)(NaUserContext *user);
//    void FSUB(na_user_initialize)(NaUserContext *user);
//    void FSUB(na_initialize)(NaUserContext *user,double *ranget,double *all_misfits,double *all_models,double *xcur);
//    void FSUB(na_options)(NaUserContext *user,int *nsleepMax);
//    void FSUB(na_initial_sample)(NaUserContext *user,double *all_models,double *rangeo,double *all_misfits,int *nsample_r,int *nsample_init);
    void FSUB(transform2raw)(NaUserContext *user,double *all_models,double *current_model);
    void FSUB(na_sample)(NaUserContext *user,double *all_models, int *ntot,int *ns,double *all_misfits,int *mfitord,double *ranget,double *xcur,double *dlist);
//    void FSUB(na_misfits)(NaUserContext *user,int *ns,double *misfit,int *it,int *ntot,double *mfitmin,double *mfitminc,double *mfitmean,int *mopt,int *ncells,double *work,int *ind,int *iwork,int *mfitord);
//    void FSUB(na_random)(NaUserContext *user,double *all_models,double *ranget,int *ntot);
//    void FSUB(na_display)(NaUserContext *user,double *model_opt,int *it,int *ntot,double *mfitmin,double *mfitminc,double *mfitmean,int *mopt);
    void FSUB(na_user_finalize)(NaUserContext *user);
    double FSUB(ran3)(int *seed);
    void FSUB(transform2sca)(double *raw_model, int *nd, double *ranges, double *scales, double *sca_model);


#ifdef __cplusplus
}
#endif
// === Declaration of functions ===============================================

void na(MPI_Comm opt_comm,MPI_Comm fwrd_comm);

#endif

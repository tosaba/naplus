!-----------------------------------------------------------------------------------------

subroutine print_matrix(user)

   use iso_c_binding

   implicit none

   integer i, j,m,n

!     ------------------------------------------------------------------
      include 'na_ftypes.h'
!      type(nausercontext_f) user
!     ------------------------------------------------------------------



   real(kind=8), pointer :: ranges(:,:)

   integer N2D(2)


   N2D(1) = 2
   N2D(2) = user%nd

   call c_f_pointer(user%ranges_in, ranges, N2D)

   write(*,*) 'Number of rows   : ', N2D(1)
   write(*,*) 'Number of columns: ', N2D(2)

   do i=1,N2D(1)
      write(*,*) ranges(i,1:N2D(2))
   enddo
end


! ----------------------------------------------------------------------------
!
!     NA_sample - generates a new sample of models using
!                      the Neighbourhood algorithm by distributing
!              nsample new models in ncells cells.
!
!     Comments:
!          If xcur is changed between calls then restartNA
!          must be set to true. logical restartNA must also
!          be set to true on the first call.
!
!         Calls are made to various NA_routines.
!
!                        M. Sambridge
!                         Last updated May 1999.
!
!
!     May. 2004.
!          changes made to allow NA to work in 'continuous' mode
!     June. 2004
!          changes made to allow NA to work out ncells for itself rather
!          than relying on the user. This happens if ncells is set to -1
!          Currently this only works in continuous mode with MPI.
!
!
!-----------------------------------------------------------------------
      subroutine NA_sample(user,na_models, ntot,ns,misfit, mfitord,ranget,xcur,dlist)
!     ------------------------------------------------------------------
      use iso_c_binding
      implicit none
!     ------------------------------------------------------------------
      include 'na_ftypes.h'


!     ------------------------------------------------------------------
      integer               :: ntot,i,kd,iw,il,mopt,nodex,ns
      integer               :: cell,nsampercell,nrem,stepsize,id
      integer               :: ind_cell,ind_cellnext
      real(kind=8)          :: dist,x1,x2,dminx
      real(kind=8)          :: na_models(user%nd,*)
      real(kind=8)          :: ranget(2,user%nd)
      real(kind=8)          :: misfit(*)
      real(kind=8)          :: xcur(*)
      real(kind=8)          :: dlist(*)
      integer               :: mfitord(*)
      logical               :: resetlist

!     ------------------------------------------------------------------
      save id
!     ------------------------------------------------------------------


    ! choose initial axis randomly ==> nainit_idnext
    !call irandomvalue(1,user%nd,user%nainit_idnext,user%sobol,user%iseed)
    call prng_unfintrandab(user%prngstream,user%opt_rank,1,user%nd, user%nainit_idnext)


    ! ???? still not sure what this is
!    user%nainit_ic = user%nainit_ic + 1
!    This is not necessary because restartNa is test anyways
!    if(mod(user%nainit_ic,user%nclean)==0)resetlist = .true.
!    JUNE 2014 : THIS IS DONE EVERY 500 MODELS SINCE NCLEAN IS 500

    ! We resample ncells with ns samples. How many samples are left
    nrem = mod(abs(ns),user%ncells)

    ! Decide on how many samples are sampled per cell
    if(nrem==0)then
        nsampercell = abs(ns)/user%ncells
    else
        nsampercell = 1+abs(ns)/user%ncells
    end if

    ! Random integer between 1 and user%ncell: choose ONE of the ncell best cells ==> cell
    !call irandomvalue(1, user%ncells, cell,user%sobol,user%iseed)
    call prng_unfintrandab(user%prngstream,user%opt_rank,1,user%ncells, cell)

    ! and get index of this cell ==> ind_cellnext
    ind_cellnext = mfitord(cell)

    ! nsampercell samples per cell: choose ONE in terms of a stepsize ==> stepsize
    !call irandomvalue(1, nsampercell, stepsize,user%sobol,user%iseed)
    call prng_unfintrandab(user%prngstream,user%opt_rank,1,nsampercell, stepsize)

    ! Choose Voronoi cell for sampling
    ind_cell = ind_cellnext

!   Reset walk to chosen model
!   choose a cell (ind_cell) and put parameters to xcur: xcur(1:nd)=na_models(1:nd,ind_cells);
!   restartNA ist set to 1
    call NA_restart(na_models,user%nd,ind_cell,xcur,0,user%restartNA)


    if(user%restartNA==1)then
        resetlist = .true.
        user%restartNA = 0
    end if

!   Perform random walk
    do il = 1,stepsize
        do iw = 1,user%nd

        ! Update dlist and node for new axis
        if(.not.resetlist)then
          ! Incremental update
          call NNupdate_dlist(user%nainit_idnext,id,dlist,na_models,user%nd,ntot,xcur,nodex,dminx)
        else
          ! Full update
          call NNcalc_dlist(user%nainit_idnext,dlist,na_models,user%nd,ntot,xcur,nodex,dminx)
          resetlist = .false.
        end if

        id = user%nainit_idnext

        ! Calculate intersection of current Voronoi cell with current 1-D axis
        call NNaxis_intersect(xcur,id,dlist,na_models,user%nd,ntot,nodex,ranget(1,id),ranget(2,id),x1,x2)

        ! Generate new node in Voronoi cell of input point
        !kd = id + (cell-1)*user%nd
        !call NA_deviate (x1,x2,kd,xcur(id),user%sobol,user%iseed,user%debug)
        call prng_unfrandab(user%prngstream,user%opt_rank,x1,x2,xcur(id))


        ! Increment axis
        user%nainit_idnext = user%nainit_idnext + 1
        if(user%nainit_idnext>user%nd)user%nainit_idnext=1

        end do
    end do


    ! Put new sample in list
    do i=1,user%nd
        na_models(i,ntot+1) = xcur(i)
    end do

    return
    end

!
! ----------------------------------------------------------------------
!
!     NA_restart - resets NA walk to start from input model.
!
!     Calls no other routines.
!
!                     M. Sambridge, Oct. 1996
!
! ----------------------------------------------------------------------
!
      subroutine NA_restart(na_models,nd,mreset,x,debug,restartNA)
!     -------------
      implicit none
!     arguments:
      integer  nd,mreset
!     other:
      integer  k,i
!     -------------
      real(kind=8)            na_models(nd,*)
      real(kind=8)            x(*)

      integer         restartNA
      integer         debug
!
      if(debug==1)then
         write(*,*)' NA_restart: reset to model ',mreset
         write(*,*)' current model on entry '
         write(*,*)(x(k),k=1,nd)
      end if

      do i=1,nd
        x(i) = na_models(i,mreset)
      end do

      restartNA = 1

      if(debug==1)then
         write(*,*)' current model on exit '
         write(*,*)(x(k),k=1,nd)
      end if

      return
      end

!
!-----------------------------------------------------------------------
!
!     NNaxis_intersect - find intersections of current Voronoi cell
!              with current 1-D axis.
!
!     Input:
!       x(nd)     :point on axis
!       dim       :dimension index (defines axis)
!       dlist     :set of distances of base points to axis
!       bp(nd,nb) :set of base points
!       nd        :number of dimensions
!       nb        :number of base points
!       resetlist :TRUE if dlist and nodex is to be calculated
!       nodex     :index of base node closest to x
!       dmin_in   :distance of base node closest to x
!       xmin      :start point along axis
!       xmax      :end point along axis
!
!     Output:
!       x1        :intersection of first Voronoi boundary
!       x2        :intersection of second Voronoi boundary
!
!     Comment:
!     This method uses a simple formula to exactly calculate
!     the intersections of the Voronoi cells with the 1-D axis.
!     It makes use of the perpendicluar distances of all nodes
!     to the current axis contained in the array dlist.
!
!         The method involves a loop over ensemble nodes for
!     each new intersection found. For an axis intersected
!     by ni Voronoi cells the run time is proportional to ni*ne.
!
!     It is assumed that the input point x(nd) lies in
!     the Vcell of nodex, i.e. nodex is the closest node to x(nd).
!
!     Note: If the intersection points are outside of either
!           axis range then the axis range is returned, i.e.
!
!                 x1 is set to max(x1,xmin) and
!                 x2 is set to min(x2,xmin) and
!
!                                     M. Sambridge, RSES, June 1998
!
!-----------------------------------------------------------------------
!
      subroutine NNaxis_intersect(x,dim,dlist,bp,nd,nb,nodex,xmin,xmax,x1,x2)
!     -------------
      implicit none
!     arguments:
      integer  nd,nodex,nb
      real(kind=8)     x1,x2,xmin,xmax
!     other:
      real(kind=8)     x0,xc,xi,dp0,dx,dpc
      integer  j
!     -------------
      real(kind=8)          x(nd)
      real(kind=8)          bp(nd,nb)
      real(kind=8)          dlist(nb)
      integer         dim
!
!     search through nodes
      x1 = xmin
      x2 = xmax
      dp0   = dlist(nodex)
      x0    = bp(dim,nodex)

!     write(*,*)
!     write(*,*)'k ',k,' xmin ',xmin,' xmax ',xmax,' x0',x0
!     write(*,*)'node0 = ',node0
!     write(*,*)'left = ',left
!     write(*,*)'right = ',right
!
!     find intersection of current Voronoi cell with 1-D axis
      do j=1,nodex-1
         xc    = bp(dim,j)
         dpc   = dlist(j)

!        calculate intersection of interface (between nodes nodex and j) and 1-D axis.
         dx = x0 - xc
         if(dx.ne.0.0)then
            xi = 0.5*(x0+xc+(dp0-dpc)/dx)
            if(xi.gt.xmin.and.xi.lt.xmax)then
               if(xi.gt.x1.and.x0.gt.xc)then
                  x1 = xi
               else if(xi.lt.x2.and.x0.lt.xc)then
                  x2 = xi
               end if
            end if
         end if
      end do

      do j=nodex+1,nb
         xc    = bp(dim,j)
         dpc   = dlist(j)
!        calculate intersection of interface (between nodes nodex and j) and 1-D axis.
         dx = x0 - xc
         if(dx.ne.0.0)then
            xi = 0.5*(x0+xc+(dp0-dpc)/dx)
            if(xi.gt.xmin.and.xi.lt.xmax)then
               if(xi.gt.x1.and.x0.gt.xc)then
                  x1 = xi
               else if(xi.lt.x2.and.x0.lt.xc)then
                  x2 = xi
               end if
            end if
         end if
      end do

      return
      end
!
!-----------------------------------------------------------------------
!
!     subroutine NNcalc_dlist - calculates square of distance from
!                               all base points to new axis (defined
!                               by dimension dim through point x.
!                               It also updates the nearest node and
!                               distance to the point x.
!
!     This is a full update of dlist, i.e. not using a previous dlist.
!
!-----------------------------------------------------------------------
!
      subroutine NNcalc_dlist(dim,dlist,bp,nd,nb,x,nodex,dminx)
!     -------------
      implicit none
!     arguments:
      integer  nd,nodex,nb
      real(kind=8)     dminx
!     other:
      real(kind=8)     dmin,dsum,dnodex,d
      integer  i,j
!     -------------
      real(kind=8)          bp(nd,*)
      real(kind=8)          x(nd)
      real(kind=8)          dlist(*)
      integer         dim

      dmin = 0.
      do j=1,dim-1
         d = (x(j)-bp(j,1))
         d = d*d
         dmin = dmin + d
      end do
      do j=dim+1,nd
         d = (x(j)-bp(j,1))
         d = d*d
         dmin = dmin + d
      end do
      dlist(1) = dmin
      d = (x(dim)-bp(dim,1))
      d = d*d
      dmin = dmin + d
      nodex = 1
!
      do i=2,nb
         dsum = 0.
         do j=1,dim-1
            d = (x(j)-bp(j,i))
            d = d*d
            dsum = dsum + d
         end do
         do j=dim+1,nd
            d = (x(j)-bp(j,i))
            d = d*d
            dsum = dsum + d
         end do
         dlist(i) = dsum
         d = (x(dim)-bp(dim,i))
         d = d*d
         dsum = dsum + d
         if(dmin.gt.dsum)then
            dmin = dsum
            nodex = i
         end if
         dnodex = dmin
      end do
!
!     write(*,*)' resetlist'
!     write(*,*)' dlist',(dlist(i),i=1,nb)
!     write(*,*)' input nodex',nodex
!
      return
      end
!
!-----------------------------------------------------------------------
!
!     subroutine NNupdate_dlist - calculates square of distance from
!                    all base points to new axis, assuming
!                    dlist contains square of all distances
!                    to previous axis dimlast. It also
!                    updates the nearest node to the
!                    point x through which the axes pass.
!
!
!-----------------------------------------------------------------------
!
      subroutine NNupdate_dlist(dim,dimlast,dlist,bp,nd,nb,x,node,dmin)
!     -------------
      implicit none
!     arguments:
      integer  nd,node,nb
      real(kind=8)     dmin
!     other:
      real(kind=8)     d2,ds,d1
      integer  i
!     -------------
      real(kind=8)          bp(nd,*)
      real(kind=8)          x(nd)
      real(kind=8)          dlist(*)
      integer         dim,dimlast

      d1 = (x(dimlast)-bp(dimlast,1))
      d1 = d1*d1
      dmin = dlist(1)+d1
      node = 1
      d2 = (x(dim)-bp(dim,1))
      d2 = d2*d2
      dlist(1) = dmin-d2
      do i=2,nb
         d1 = (x(dimlast)-bp(dimlast,i))
         ds = d1
         d1 = dlist(i)+d1*d1
         if(dmin.gt.d1)then
            dmin = d1
            node = i
         end if
         d2 = (x(dim)-bp(dim,i))
         d2 = d2*d2
         dlist(i) = d1-d2
!        if(i.eq.nb)then
!           write(*,*)' NNupdate_dlist: node 20'
!           write(*,*)' dlist ',dlist(i)
!           write(*,*)' dim ',dim
!           write(*,*)' dimlast ',dimlast
!           write(*,*)' x ',x(1),x(2)
!           write(*,*)' bp ',bp(1,20),bp(2,20)
!           write(*,*)' d1 ',ds
!           write(*,*)' d2 ',d2
!        end if

      end do

      return
      end
!
!
! ----------------------------------------------------------------------
!
!     transform2raw - transforms model from scaled to raw units.
!
!     Input:
!           nd        : dimension of parameter space
!           model_sca(nd) : model in scaled co-ordinates
!           ranges(2,nd)   : min and max of parameter space
!                   in raw co-ordinates.
!           scales(nd+1)  : range scale factors
!
!     Output:
!           model_raw(nd) : model in scaled co-ordinates
!
!     Comments:
!              This routine transforms a model in dimensionless scaled
!          co-ordinates to input (raw) units.
!
!         Calls no other routines.
!
!                                        M. Sambridge, March 1998
!
! ----------------------------------------------------------------------
!
      subroutine transform2raw(user,model_sca,model_raw)
!     ------------------------------------------------------------------
      use iso_c_binding
      implicit none
!     ------------------------------------------------------------------
      include 'na_ftypes.h'
!      type(nausercontext_f) user
!     ------------------------------------------------------------------
      integer               :: i,N2D(2),N1D(1)
      real(kind=8)          :: a,b
      real(kind=8)          :: model_raw(user%nd)
      real(kind=8)          :: model_sca(user%nd)
      real(kind=8), pointer :: ranges(:,:)
      real(kind=8), pointer :: scales(:)
!     ------------------------------------------------------------------

!     Get access to arraysof user struct
      N2D(1) = 2
      N2D(2) = user%nd
      call c_f_pointer(user%ranges_in, ranges, N2D)
      N1D(1) = (user%nd)+1
      call c_f_pointer(user%scales_in, scales, N1D)

      if(scales(1)==0.0)then
         do i=1,user%nd
            model_raw(i) = model_sca(i)
         end do
      else if(scales(1)==-1.0)then
         do i=1,user%nd
            b = model_sca(i)
            a = 1-b
            model_raw(i) = a*ranges(1,i) + b*ranges(2,i)
         end do
      else
         do i=1,user%nd
            model_raw(i) = ranges(1,i) + scales(i+1)*model_sca(i)
         end do
      end if
!
      return
      end
!
! ----------------------------------------------------------------------
!
!     transform2sca - transforms model from raw to scaled units.
!
!     Input:
!           nd        : dimension of parameter space
!           model_raw(nd) : model in raw co-ordinates
!           ranges(2,nd)   : min and max of parameter space
!                   in raw co-ordinates.
!           scales(nd+1)  : range scale factors
!
!     Output:
!           model_sca(nd) : model in scaled co-ordinates
!
!     Comments:
!              This routine transforms a model in raw co-ordinates
!          to dimensionless units determined by parameter scale factors.
!
!         Calls no other routines.
!
!                                            M. Sambridge, March 1998
!
! ----------------------------------------------------------------------
!
      subroutine transform2sca(model_raw,nd,ranges,scales,model_sca)
!     -------------
      implicit none
!     -------------
      integer      :: i,nd
      real(kind=8) :: model_sca(nd)
      real(kind=8) :: model_raw(nd)
      real(kind=8) :: scales(nd+1)
      real(kind=8) :: ranges(2,nd)
!
      if(scales(1).eq.0.0)then
         do i=1,nd
            model_sca(i) = model_raw(i)
         end do
      else if(scales(1).eq.-1.0)then
         do i=1,nd
            model_sca(i) = (model_raw(i)-ranges(1,i))/(ranges(2,i)-ranges(1,i))
         end do
      else
         do i=1,nd
            model_sca(i) = (model_raw(i)-ranges(1,i))/scales(i+1)
         end do
      end if
!
      return
      end
!
!=======================================================================
!     NUMERICAL RECIPES ROUTINES
!=======================================================================

!
!-----------------------------------------------------------------------
!
!     Numerical recipes routine na_indexx
!
!-----------------------------------------------------------------------
!
      subroutine na_indexx(n,arr,indx)
!     -------------
      implicit none
!     -------------
      integer n,indx(n),m,nstack
      real(kind=8) arr(n)
      parameter (m=7,nstack=50)
      integer i,indxt,ir,itemp,j,jstack,k,l,istack(nstack)
      real(kind=8) a
      do 11 j=1,n
        indx(j)=j
11    continue
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.m)then
        do 13 j=l+1,ir
          indxt=indx(j)
          a=arr(indxt)
          do 12 i=j-1,1,-1
            if(arr(indx(i)).le.a)goto 2
            indx(i+1)=indx(i)
12        continue
          i=0
2         indx(i+1)=indxt
13      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        itemp=indx(k)
        indx(k)=indx(l+1)
        indx(l+1)=itemp
        if(arr(indx(l+1)).gt.arr(indx(ir)))then
          itemp=indx(l+1)
          indx(l+1)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l)).gt.arr(indx(ir)))then
          itemp=indx(l)
          indx(l)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l+1)).gt.arr(indx(l)))then
          itemp=indx(l+1)
          indx(l+1)=indx(l)
          indx(l)=itemp
        endif
        i=l+1
        j=ir
        indxt=indx(l)
        a=arr(indxt)
3       continue
          i=i+1
        if(arr(indx(i)).lt.a)goto 3
4       continue
          j=j-1
        if(arr(indx(j)).gt.a)goto 4
        if(j.lt.i)goto 5
        itemp=indx(i)
        indx(i)=indx(j)
        indx(j)=itemp
        goto 3
5       indx(l)=indx(j)
        indx(j)=indxt
        jstack=jstack+2
        if(jstack.gt.nstack) write(*,*) '[ERROR in na_indexx] nstack too small in na_indexx'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      end
!
!=======================================================================
!     SUB FUNCTIONS
!=======================================================================
!
!-----------------------------------------------------------------------
!
!     Numerical recipes routine adapted to give ind and iselect
!
!-----------------------------------------------------------------------
!
      FUNCTION na_select(k,n,arr,ind,iselect)
!     -------------
      implicit none
!     arguments:
      integer  iselect
!     other:
      integer itemp,ia
!     -------------
      integer k,n
      real(kind=8) na_select,arr(n)
      integer ind(n)
      integer i,ir,j,l,mid
      real(kind=8) a,temp
      l=1
      ir=n
1     if(ir-l.le.1)then
        if(ir-l.eq.1)then
          if(arr(ir).lt.arr(l))then
            temp=arr(l)
            arr(l)=arr(ir)
            arr(ir)=temp
            itemp=ind(l)
            ind(l)=ind(ir)
            ind(ir)=itemp
          endif
        endif
        na_select=arr(k)
        iselect=ind(k)
        return
      else
        mid=(l+ir)/2
        temp=arr(mid)
        arr(mid)=arr(l+1)
        arr(l+1)=temp
        itemp=ind(mid)
        ind(mid)=ind(l+1)
        ind(l+1)=itemp
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
          itemp=ind(l+1)
          ind(l+1)=ind(ir)
          ind(ir)=itemp
        endif
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
          itemp=ind(l)
          ind(l)=ind(ir)
          ind(ir)=itemp
        endif
        if(arr(l+1).gt.arr(l))then
          temp=arr(l+1)
          arr(l+1)=arr(l)
          arr(l)=temp
          itemp=ind(l+1)
          ind(l+1)=ind(l)
          ind(l)=itemp
        endif
        i=l+1
        j=ir
        a=arr(l)
        ia=ind(l)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        itemp=ind(i)
        ind(i)=ind(j)
        ind(j)=itemp
        goto 3
5       arr(l)=arr(j)
        arr(j)=a
        ind(l)=ind(j)
        ind(j)=ia
        if(j.ge.k)ir=j-1
        if(j.le.k)l=i
      endif
      goto 1
      end


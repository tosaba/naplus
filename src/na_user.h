#ifndef na_user_H
#define na_user_H

// --- system include ---------------------------------------------------------
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


#ifdef GSL
#include <gsl/gsl_qrng.h>
#include <gsl/gsl_rng.h>
#endif
#include <iostream>
using std::cout;
using std::endl;
// --- local include ----------------------------------------------------------
#include "na_types.h"


// --- definitions ------------------------------------------------------------
#define MAX_LINE_LEN 5000
#define _TRUE 1
#define _FALSE 0


const char comments[] = "#!|";

// === Declaration of fortran sub functions ===================================
#ifdef __cplusplus
extern "C" {
#endif

    double FSUB(na_select)(int *k,int *n,double *arr,int *ind,int *iselect);
    void FSUB(na_jumble)(int *iarr,double *arr, int *n,int *iseed);
    void FSUB(na_indexx)(int *n,double *arr, int *indx);
    void FSUB(transform2sca)(double *raw_model, int *nd, double *ranges, double *scales, double *sca_model);

	void prng_unfrand01(RngStream *stream, int rank,double *rnd);
	void prng_unfrandab(RngStream *stream   , int rank, double a, double b, double *rnd);
	void prng_unfintrandab(RngStream *stream, int rank, int    a, int    b, int    *rnd);
#ifdef __cplusplus
}
#endif
//-----------------------------------------------------------------------------
    

void PRNGCreate(NaUserContext &user);
void PRNGDestroy(NaUserContext &user);
void PRNGAdvanceState(NaUserContext &user);
void PRNGReset(NaUserContext &user);
void PRNGUnfRand01(RngStream *stream   , int rank, double *rnd);
void PRNGUnfRandAB(RngStream *stream   , int rank, double a, double b, double *rnd);
void PRNGUnfIntRandAB(RngStream *stream, int rank, int    a, int    b, int    *rnd);
void DiscParamRange(NaUserContext &user,double *discrange);




void check_err(MPI_Comm comm, int err_code);
double * make_matrix(int m, int n);
void UserContext_initialize(NaUserContext &user);
void UserContext_finalize(NaUserContext &user);
void LoadParamFile(NaUserContext &user);
void write_models(NaUserContext &user,NaCurrentResult &cr,double mCount,double mID,int sourceID, const char *prefix);
void write_header(NaUserContext &user,const char *prefix);
void read_models(NaUserContext &user,NaCurrentResult &cr,double *all_models,double *all_misfits,const char *prefix);
//void GetQuasiRandomSequence(NaUserContext &user,double *all_models_init,double *ranget);
//void GetPseudoRandomSequence(NaUserContext &user,double *all_models_init,double *ranget);
//void GetDiscretePseudoRandomSequence(NaUserContext &user,double *all_models_init,double *ranget);

char *trim(char *str);
int is_comment_line( const char line[] );
void strip( char line[] );
void strip_L( char str[] );
void trim_past_comment( char line[] );
void strip_all_whitespace( char str[], char str2[] );
int key_matches( const char key[], char line[] );
void parse_GetInt( FILE *fp, const char key[], int *value, int *found );
void parse_GetIntAllInstances( FILE *fp, const char key[], int *nvalues, int values[], int max_L, int *found );
void parse_GetDouble( FILE *fp, const char key[], double *value, int *found );
void parse_GetDoubleAllInstances( FILE *fp, const char key[], int *nvalues, double values[], int max_L, int *found );
void parse_GetDoubleArray( FILE *fp, const char key[], int *nvalues, double values[], int *found );
void parse_GetString( FILE *fp, const char key[], char value[], int max_L, int *found );
//-----------------------------------------------------------------------------
float** Create2dArray_float(int arraySizeX, int arraySizeY);
void Destroy2dArray_float(float ** myArray, int nx);
float** Create2dArray_float_cpp(int m, int n);

void na_initialize(NaUserContext &user, double *ranget, double *all_misfits, double *all_models, double *xcur);
void na_misfit(NaUserContext &user,NaMfitStatistics &stats,double *all_misfits,int ntot,int nsample,int it,int ncells);
void na_shuffle(NaUserContext &user, int *iarr,double *arr,int n);

void stats_initialize(NaUserContext &use,NaMfitStatistics &stats);
void stats_finalize(NaMfitStatistics &stats);
void wbuff_initialize(NaUserContext &user,NaWriteBuffer &wbuff);
void wbuff_finalize(NaWriteBuffer &wbuff);
void buffwrite_models(NaUserContext &user,NaWriteBuffer &wbuf,NaCurrentResult &cr,double mCount,double mID,int sourceID, const char *prefix);

//-----------------------------------------------------------------------------
template <class T>
void ByteSwap(T *buff,int n){
    int      i,j;
    T        tmp=0,*buff1 = (T*)buff;
    char    *ptr1,*ptr2 = (char*)&tmp;

    for (j=0; j<n; j++) {
        ptr1 = (char*)(buff1 + j);
        for (i=0; i<(int)sizeof(T); i++) {
            ptr2[i] = ptr1[sizeof(T)-1-i];
        }
        for (i=0; i<(int) sizeof(T); i++) {
            ptr1[i] = ptr2[i];
        }
    }
    return;
}
//--- template functions ------------------------------------------------------
/*
 *  we always write BIG ENDIAN to binary and read BIG ENDIAN from binary
 */
template <class T>
void read_array(T *array,int n,int *position,FILE *fileBIN){
    const int num=1;
    size_t readcnt;
    readcnt = fread(array,sizeof(T),n, fileBIN);
    *position += readcnt*sizeof(T);
    if(IS_LITTLE_ENDIAN){
        ByteSwap(array,n);
    }
    return;
}

//-----------------------------------------------------------------------------
//template <class T>
//void write_array(T *array,int n,int *position,FILE *fileBIN){
//  size_t readcnt;
//  readcnt = fread(array,sizeof(T),n, fileBIN);
//  *position += readcnt*sizeof(T);
//  return;
//-----------------------------------------------------------------------------
template <class T>
void print_array(T *array, int length){
    for (int n=0; n<length; n++)
        cout << array[n] << " ";
    cout << endl;
    //for (int n=0; n<length; n++)
    //  printf("%g ", array[n]);
    //printf("\n");
}
//-----------------------------------------------------------------------------

template <class T>
void zeromem(T *& a, const int n = 1)
{
	// clear memory
	memset(a, 0, sizeof(T)*n);
};
#endif

#ifndef naplus_H
#define naplus_H

// --- system include ---------------------------------------------------------
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
using namespace std;

// --- local include ----------------------------------------------------------
#ifdef LaMEM
#include "petsc.h"
#endif
#include "naplus_utils.h"
#include "na_forward.h"
#include "Version.h"
#include "naplus_globalvars.h"

// --- declaration of functions -----------------------------------------------
void na(MPI_Comm opt_comm,MPI_Comm fwrd_comm);

#endif

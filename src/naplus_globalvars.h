#ifndef naplus_globalvars_H
#define naplus_globalvars_H


typedef struct{
	int      DensN  ,ViscN;
	double   DensRef;
	int     *DensIdx,*ViscIdx;
	char     fileID[20];
	int      FwrdModel;
} NAplusContext;

extern NAplusContext naplus;

#endif

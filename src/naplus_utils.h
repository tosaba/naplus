#ifndef naplus_utils_H
#define naplus_utils_H

// --- system include ---------------------------------------------------------
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// --- local include ----------------------------------------------------------
#include "na_user.h"
#include "naplus_globalvars.h"

// --- declaration of functions -----------------------------------------------
void communicate_task(MPI_Comm fwrd_comm,int *task);
void check_commandline(MPI_Comm comm, int argc,char *argv[],int *fwrd_group_size,int *fwrd_group_num);
void load_inputfile(MPI_Comm comm);
void MPI_Comm_split_host(MPI_Comm comm_in, MPI_Comm *comm_host);
#endif

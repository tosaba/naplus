#!/bin/bash
# 
# ./ValgrindCheck.sh 4 "./na_app -arg ..." <suffix>
#
#
echo $2
MPIWRAP_DEBUG=quiet \
LD_PRELOAD=/opt/valgrind/lib/valgrind/libmpiwrap-amd64-linux.so \
mpirun -np $1 \
valgrind -v \
--leak-check=full \
--track-origins=yes \
--show-reachable=yes \
--xml=yes \
--xml-file=log_$3.xml \
--child-silent-after-fork=yes \
-q \
$2 \
a.out > log_$3.txt 2>&1


exit

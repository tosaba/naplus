#include "bin2ascii.h"
// ============================================================================
int main (int argc, char *argv[])
{
	int rank;
	char *prefix;
	
		
	check_commandline(argc,argv,&rank,&prefix);
	bin2ascii_main(rank,prefix);
	free(prefix);

	return 0;
}
// ============================================================================
void check_commandline(int argc,char *argv[],int *rank,char **prefix)
{
	// argv[0] == name of program
	if(argc<2){
		printf("[bin2ascii error] Not enough command line arguments.\n");
		exit(1);		
	}
	else if(argc==2){
		if(strcmp(argv[1],"-help")==0){
			printf("%s",help);
			exit(1);
		}
		else{
			printf("[bin2ascii error] Not enough command line arguments.\n");
			exit(1);
		}
	}
	else if(argc==5){
		if(strcmp(argv[1],"-rank")==0 && strcmp(argv[3],"-prefix")==0){
			*rank = atoi(argv[2]);
			if(asprintf(prefix,"%s",argv[4])<0) printf("[NAplus Error] asprintf in funct check_commandline");
			
			if(strcmp(*prefix,"own")!=0 && strcmp(*prefix,"all")!=0 && strcmp(*prefix,"merge")!=0){
				printf("[bin2ascii error] Unknown prefix.\n");
				printf("[bin2ascii error] prefix: %s\n",*prefix);				
				exit(1);
			}
		}
		else{
			printf("[bin2ascii error] Unknown command line arguments.\n");
			printf("[bin2ascii error] -rank ,-Ndim, -prefix should be used.\n");			
			exit(1);		
		}
	}
	else{
		printf("[bin2ascii error] Wrong number of command line arguments: %d.\n",argc);
		exit(1);
	}
	return;
}
//-----------------------------------------------------------------------------
void bin2ascii_main(int rank, char *prefix){
/* format:
 * | cntModels | cntModels_own | source | param1 | param2 | ... | paramN | misfitval | ctime |
 *
 */
	FILE   *fileBIN,*fileTXT;
	double *buffer,*ranges_in;
	char   *filename;
	int     filesize;
	int     Nbuffer,Ndim;
	int     position,Nmodels,headersize;
	int     restart,opt_rank,opt_size,Nheader;


	// open binary to read
	if(strcmp(prefix,"merge")==0){
		if(asprintf(&filename,"models_%s.bin",prefix)<0) printf("[NAplus Error] asprintf in funct bin2ascii_main");
	}
	else{
		if(asprintf(&filename,"models_%s_ID%d.bin",prefix,rank)<0) printf("[NAplus Error] asprintf in funct bin2ascii_main");
	}
	fileBIN = fopen(filename,"r");
	if (fileBIN == NULL) {
  		fprintf(stderr, "Can't open file: %s \n",filename);
  		exit(1);
  	}
	free(filename);
	
	// open ascii to write
	if(strcmp(prefix,"merge")==0){
		if(asprintf(&filename,"models_%s.txt",prefix)<0) printf("[NAplus Error] asprintf in funct bin2ascii_main");
		printf("> create file: %s\n",filename);
	}
	else{
		if(asprintf(&filename,"models_%s_ID%d.txt",prefix,rank)<0) printf("[NAplus Error] asprintf in funct bin2ascii_main");
		printf("> create file: %s\n",filename);
	}
	fileTXT = fopen(filename,"w");
	if (fileTXT == NULL) {
  		fprintf(stderr, "Can't open file: %s \n",filename);
  		exit(1);
  	}
	free(filename);	
	
	// obtain binary file size
	fseek (fileBIN , 0 , SEEK_END);
	filesize = (int) ftell(fileBIN);
	rewind (fileBIN);

	// Read header information (there is no need for opt_rank and opt_size yet)
	position = 0;
	Nheader = 4;
	read_array(&restart,1,&position,fileBIN);
	read_array(&opt_rank,1,&position,fileBIN);
	read_array(&opt_size,1,&position,fileBIN);
	read_array(&Ndim,1,&position,fileBIN);
	ranges_in = new double[2*Ndim];
	read_array(ranges_in,2*Ndim,&position,fileBIN);
	headersize = Nheader*(int)sizeof(int) + 2*Ndim*(int)sizeof(double);

	// =============
	Nbuffer=Ndim+5;
	// =============

	// determine the number of models (all are of double precision)
	Nmodels = (filesize-headersize)/(int)sizeof(double)/(Nbuffer);
	printf("> number of models: %d\n",Nmodels);

	// allocate buffer to read a full model + misfit + modelID
	buffer = new double[Nbuffer];

	// write header
	fprintf(fileTXT,"ndim = %d \n",Ndim);
	fprintf(fileTXT,"ranges =");
	for(int k=0;k<(Ndim*2);k++){
		fprintf(fileTXT," %g",ranges_in[k]);
		if(k==(Ndim*2)-1)
			fprintf(fileTXT,"\n");
	}

	// read binary data / write ascii data
	while(position<filesize){
		read_array(buffer,Nbuffer,&position,fileBIN);
		write_ascii(Ndim,Nmodels,buffer,fileTXT);
	}

	// deallocate buffer
	delete[] buffer;
	delete[] ranges_in;

	// close files
	fclose(fileBIN);
	fclose(fileTXT);
	
	return;
}
//-----------------------------------------------------------------------------
void write_ascii(int Ndim,int Nmodels,double *buffer,FILE *fileTXT){
/*
 * http://en.wikipedia.org/wiki/Printf_format_string#Format_placeholders
 */	
	int k,n;
	char *format;

	// model ID	
	//n = numDigits(Nmodels);
	n = numDigits(10e8);
	// no padding with zeros
	if(asprintf(&format,"%%%dd ",n)<0) printf("[NAplus Error] asprintf in funct write_ascii");
	//padding with zeros
//	if(asprintf(&format,"%%0%dd ",n)<0) printf("[NAplus Error] asprintf in funct write_ascii");

	fprintf (fileTXT,format,(int)buffer[0]);// cntModels
	fprintf (fileTXT,format,(int)buffer[1]);// cntModels_own
	fprintf (fileTXT,format,(int)buffer[2]);// source
	free(format);

	// model parameters	
	for(k=3;k<=Ndim+2;k++)
		fprintf (fileTXT,"% 20.20e ",buffer[k]);

	// misfit
	fprintf (fileTXT,"% 20.20e ",buffer[Ndim+3]);

	// time
	fprintf (fileTXT,"% 20.20e \n",buffer[Ndim+4]);

	return;
}
//-----------------------------------------------------------------------------
int numDigits(int number)
{
    int digits = 0;
    if (number < 0) digits = 1; // remove this line if '-' counts as a digit
    while (number) {
        number /= 10;
        digits++;
    }
    return digits;
}

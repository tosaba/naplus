#ifndef bin2ascii_H
#define bin2ascii_H

// --- system include ---------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//-----------------------------------------------------------------------------
// Fortran function calling macros (with and without underscore)
#ifdef GNU_COMPILER
#define FSUB(a) a ## _
#endif

#ifdef IBM_XL_COMPILER
#define FSUB(a) a
#endif

// --- function declarations --------------------------------------------------
void bin2ascii_main(int rank,char *prefix);
void check_commandline(int argc,char *argv[],int *rank,char **prefix);
void write_ascii(int Ndim,int Nmodels,double *buffer,FILE *fileTXT);
int numDigits(int number);

#ifdef __cplusplus
extern "C" {
#endif

	void FSUB(write_nad_file)(int *Ndim,int *Nmodels,float *rngs,float *models,float *mfits);

#ifdef __cplusplus	
}
#endif
// --- macros -----------------------------------------------------------------
// byte order
#define IS_LITTLE_ENDIAN ( (*(char*)&num) == 1 )


//--- template functions ------------------------------------------------------
template <class T>
void ByteSwap(T *buff,int n){
	int      i,j;
	T        tmp=0,*buff1 = (T*)buff;
	char    *ptr1,*ptr2 = (char*)&tmp;
    
	for (j=0; j<n; j++) {
		ptr1 = (char*)(buff1 + j);
		for (i=0; i<(int)sizeof(T); i++) {
			ptr2[i] = ptr1[sizeof(T)-1-i];
		}
		for (i=0; i<(int) sizeof(T); i++) {
			ptr1[i] = ptr2[i];
		}
	}
	return;
}
//-----------------------------------------------------------------------------
template <class T>
void read_array(T *array,int n,int *position,FILE *fileBIN){
	const int num=1;
	size_t readcnt;
	readcnt = fread(array,sizeof(T),n, fileBIN);
	*position += readcnt*sizeof(T);
	if(IS_LITTLE_ENDIAN){
		ByteSwap(array,n);
	}
	return;
}
//-----------------------------------------------------------------------------
template <class T>
void print_array(T *array, int length){
	for (int n=0; n<length; n++)
		cout << array[n] << " ";
	cout << endl;
}




// --- help -------------------------------------------------------------------
static char help[] = "\n\
(1) bin2ascii reads binary file of the format: \n\
    | cntModels | cntModels_own | source  | param1 | param2 | ... | paramN | misfitval | ctime \n\
\n\
(2) You need 2 parameters : \n\
    -rank (integer), -prefix ('own', 'all' or 'merge')\n\
\n";
#endif

/*
 * mergebin.cpp
 *
 *  Created on: 18.12.2012
 *      Author: tobibaumann
 */

#include "binmerge.h"




// ============================================================================
int main (int argc, char *argv[])
{
	int rank,Ndim;
	char *prefix;


	//check_commandline(argc,argv,&rank,&Ndim,&prefix);
	//Ndim=3;
	binmerge_main();
	//free(prefix);

	return 0;
}

// ============================================================================
void binmerge_main(){
	DIR           *Dir;
	struct dirent *ep;
	FILE          *fileBIN,*fileTXT,*mergeBIN;
	double        *buffer,*ranges_in;
	int            filesize,Nbuffer,Ndim_file,Ndim,cnt,cntModels;
	int            position,Nmodels,headersize,opt_rank,opt_size,Nheader;
	int            restart,restart_max=0;

	// open binary to write
	mergeBIN = fopen("models_merge.bin","w");
	if (mergeBIN == NULL) {
		fprintf(stderr, "Can't open file: models_merge.bin\n");
		exit(1);
	}

	Dir = opendir ("./");
	if (Dir == NULL)
		perror ("Couldn't open the directory");

	cntModels = 0;
	cnt       = 0;


	while (ep = readdir(Dir)){
		if (ep->d_name && strstr(ep->d_name, "models_own") && strstr(ep->d_name, ".bin")){
			// file
			cnt++;

			// open binary to read
			fileBIN = fopen(ep->d_name,"r");
			if (fileBIN == NULL) {
				fprintf(stderr, "Can't open file: %s \n",ep->d_name);
				exit(1);
			}

			// read header
			position = 0;
			Nheader = 4;
			read_array(&restart,1,&position,fileBIN);
			read_array(&opt_rank,1,&position,fileBIN);
			read_array(&opt_size,1,&position,fileBIN);
			read_array(&Ndim_file,1,&position,fileBIN);
			if(cnt==1){
				Ndim=Ndim_file;
			}
			if(Ndim!=Ndim_file)
			{
				fprintf(stderr, "[bin2ascii Error] Model dimension in file (%d) != User model dimension (%d) \n",Ndim_file,Ndim);
				exit(1);
			}
			if (restart > restart_max){
				restart_max=restart;
			}

			// close file to read
			fclose(fileBIN);
		}
	}
	(void) closedir(Dir);


	printf("> maximum restart ID: %d\n",restart_max);

	Dir = opendir ("./");
	if (Dir == NULL)
		perror ("Couldn't open the directory");

	cntModels = 0;
	cnt       = 0;

	while (ep = readdir(Dir)){

		if (ep->d_name && strstr(ep->d_name, "models_own") && strstr(ep->d_name, ".bin")){

			// file
			printf("> found file %s \n",ep->d_name);
			cnt++;

			// open binary to read
			fileBIN = fopen(ep->d_name,"r");
			if (fileBIN == NULL) {
				fprintf(stderr, "Can't open file: %s \n",ep->d_name);
				exit(1);
			}

			// obtain binary file size
			fseek (fileBIN , 0 , SEEK_END);
			filesize = (int) ftell(fileBIN);
			rewind (fileBIN);

			// read header
			position = 0;
			Nheader = 4;
			read_array(&restart,1,&position,fileBIN);
			read_array(&opt_rank,1,&position,fileBIN);
			read_array(&opt_size,1,&position,fileBIN);
			read_array(&Ndim_file,1,&position,fileBIN);
			if(cnt==1){
				printf("> number of dimensions %d\n",Ndim_file);
				Ndim=Ndim_file;
			}
			if(Ndim!=Ndim_file)
			{
				fprintf(stderr, "[bin2ascii Error] Model dimension in file (%d) != User model dimension (%d) \n",Ndim_file,Ndim);
				exit(1);
			}
			ranges_in = new double[2*Ndim];
			read_array(ranges_in,2*Ndim_file,&position,fileBIN);

			//size of header
			headersize = Nheader*(int)sizeof(int) + 2*Ndim*(int)sizeof(double);
			//printf("filesize %d\n",filesize);
			//printf("headersize %d\n",headersize);

			// ==============
			Nbuffer=Ndim+5;
			// ==============

			// Number of models
			Nmodels = (filesize-headersize)/(int)sizeof(double)/(Nbuffer);
			printf("> number of models in file: %d\n",Nmodels);
			cntModels = cntModels + Nmodels;

			// Restart ID
			printf("> restart ID: %d\n",restart);

			// write header only once
			if(cnt==1){
				printf("> ranges:\n");
				print_array(ranges_in,2*Ndim);
				printf("> write header\n");
				write_array(&restart_max,1,mergeBIN);
				write_array(&opt_rank,1,mergeBIN);
				write_array(&opt_size,1,mergeBIN);
				write_array(&Ndim_file,1,mergeBIN);
				write_array(ranges_in,2*Ndim,mergeBIN);
			}
			delete[] ranges_in;

			// read/write data
			buffer = new double[Nbuffer];
			while(position<filesize){
				read_array(buffer,Nbuffer,&position,fileBIN);
				write_array(buffer,Nbuffer,mergeBIN);
			}
			delete[] buffer;

			// close file to read
			fclose(fileBIN);
			printf("-------------------------------------------\n");
		}
	}
	(void) closedir(Dir);

	// close file to write
	fclose(mergeBIN);

	printf("There's %d files in the current directory.\n", cnt);
	printf("Merged %d models in models_merge.bin.\n", cntModels);

	return;
}

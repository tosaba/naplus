c
c-------------------------------------------------------------------------
c
c	combine - Program to combine results of independent 
c		  numerical integrations produced by NAB routines
c
c					M. Sambridge, RSES, July 1998
c
c-------------------------------------------------------------------------
c
	Program combine

	include		'nab_param.inc'

	parameter	(maxfiles=100)

        real*4          covar(ndmax+nspmax,ndmax+nspmax)
        real*4          ecovar(ndmax+nspmax,ndmax+nspmax)
        real*4          tcov(ndmax+nspmax,ndmax+nspmax)
        real*4          tecov(ndmax+nspmax,ndmax+nspmax)
        real*4          sp_int(ndmax+nspmax)
        real*4          esp_int(ndmax+nspmax)
        real*4          psr(ndmax+nspmax)
        real*4          psr_mean(ndmax+nspmax,nwmax)
        real*4          psr_cov(ndmax+nspmax,nwmax)
        real*4          psr_sum(nwmax)
        real*4          axis(ndismax,ndmax+nspmax)
        real*4          range2d(4,nmarg2Dmax)
        real*4          rangep(4,ndmax+nspmax)
        real*4          sf(ndmax+nspmax)

        real*8          marginal1D(ndismax,ndmax+nspmax)
        real*8          marginal2D(ndismax,ndismax,nmarg2Dmax)
        real*8          m2d(ndismax,ndismax)
        real*8          aval,bval,tval
        integer         nmarg(2,nmarg2Dmax)

        integer         nw(maxfiles)
        integer         nsam(maxfiles)

	logical		lmarg1,lmarg2,lspec,lcov
	logical		quasi
	logical		debug
	logical		umc

	character*256	fnme
	character*256	string
	character*256	ctype
	character*1	yesorno

	sumti = 0
	sumtj = 0
	debug = .true.
c	debug = .false.
	lu = 10

c	if(.true.)go to 20

	call zero_rarray(tcov,(ndmax+nspmax)*(ndmax+nspmax))
	call zero_rarray(tecov,(ndmax+nspmax)*(ndmax+nspmax))
	call zero_rarray(sp_int,(ndmax+nspmax))
	call zero_rarray(esp_int,(ndmax+nspmax))
	call zero_rarray(psr,(ndmax+nspmax))
	call zero_darray(m2d,(ndismax*ndismax))
	call zero_darray(marginal1D,(ndismax*(ndmax+nspmax)))
	call zero_darray(marginal2D,(ndismax*ndismax*nmarg2Dmax))

 20     continue

	umc    = .false.
	lmarg1 = .false.
	lmarg2 = .false.
	lcov   = .false.
	lspec  = .false.
	tval = 0.d0
	kk1 = 1

	read(*,*)nf
	if(nf.gt.maxfiles)stop 'too many input files'
c	write(*,*)' nf ',nf
	do i=1,nf
	   read(*,fmt='(a25)')fnme
           open(lu+i,file=fnme,status='old')
        end do

	do i=1,nf
	   k = 0
 5         read(lu+i,100,end=10)string
           k = k + 1
	   if(i.eq.1.and.k.eq.6)then
              read(string,fmt='(22x,a13)')ctype
	      umc = .true.
	      if(ctype(1:13).eq.'Neighbourhood')umc = .false.
 	      if(debug)write(*,*)' ctype ',ctype
 	      if(debug)write(*,*)' umc ',umc
           end if
	   if(i.eq.1.and.k.eq.13)then
              read(string,fmt='(35x,i16)')ne
 	      if(debug)write(*,*)' ne ',ne
           end if
	   if(i.eq.1.and.k.eq.14)then
              read(string,fmt='(35x,i16)')nd
 	      if(debug)write(*,*)' nd ',nd
           end if
	   if(k.eq.15)then
              read(string,fmt='(35x,i16)')nw(i)
	      if(debug)write(*,*)' nwalks ',nw(i)
           end if
	   if(i.eq.1.and.k.eq.16)then
              read(string,fmt='(35x,i16)')nstp
	      if(debug)write(*,*)' nstp = ',nstp
           end if
      if(i.eq.1.and.k.eq.17)then
		    read(string,fmt='(35x,i16)')nnod
    		if(debug)write(*,*)' nnodes = ',nnod
      endif
    
	   if(i.eq.1.and.k.eq.19)then
              read(string,fmt='(35x,i16)')nres
	      if(debug)write(*,*)' nres = ',nres
           end if
	   if(i.eq.1.and.k.eq.20)then
              read(string,fmt='(35x,i16)')icst
	      if(debug)write(*,*)' icst = ',icst
           end if
	   if(i.eq.1.and.k.eq.21)then
              quasi =.false.
              if(string(28:32).eq.'Quasi')quasi =.true.
	      if(debug)write(*,*)' Quasi ',quasi
	      read(string(41:72),*)iseed
	      if(debug)write(*,*)' iseed ',iseed
           end if
	   if(i.eq.1.and.k.eq.25)then
              read(string,fmt='(48x,a1)')yesorno
	      if(yesorno.eq.'y')lmarg1 = .true.
	      if(debug)write(*,*)' lmarg1 ',lmarg1
           end if
	   if(i.eq.1.and.k.eq.26)then
              read(string,fmt='(48x,a1)')yesorno
	      if(yesorno.eq.'y')lcov = .true.
	      if(debug)write(*,*)' lcov ',lcov
           end if
	   if(i.eq.1.and.k.eq.27)then
              read(string,fmt='(48x,i16)')n2d
	      if(n2d.gt.0)lmarg2 = .true.
	      if(debug)write(*,*)' n2d ',n2d
           end if
	   if(i.eq.1.and.k.eq.28)then
              read(string,fmt='(48x,i16)')nsp
	      if(debug)write(*,*)' nsp ',nsp
	      if(nsp.gt.0)lspec = .true.
           end if
	   if(i.eq.1.and.k.eq.29)then
              read(string,fmt='(48x,i16)')ndis1
	      if(debug)write(*,*)' ndis1 = ',ndis1
           end if
	   if(i.eq.1.and.k.eq.30)then
              read(string,fmt='(48x,i16)')ndis2
	      if(debug)write(*,*)' ndis2 = ',ndis2
           end if
	   if(i.eq.1.and.string(3:18).eq.'Parameter ranges')then
              read(lu+i,*)
	      k = k + 1
	      do j=1,nd
                 read(lu+i,*)idum,rangep(1,j),rangep(2,j),sf(j),
     &           rangep(3,j),rangep(4,j)
	         k = k + 1
	      end do
              read(lu+i,*)
              read(lu+i,*)
              read(lu+i,*)
              read(lu+i,*)
	      k = k + 4
	      do j=nd+1,nd+nsp
                 read(lu+i,*)idum,rangep(1,j),rangep(2,j)
	         k = k + 1
	      end do
           end if
           if(string(1:26).eq.'  Number of random samples')then
              lines = k
	   end if
	   go to 5
 10        continue

	   rewind (lu+i)
	   if(debug)write(*,*)' lines = ',lines
	   do j=1,lines-1
	      read(lu+i,*)
	   end do
	   read(lu+i,101)string
	   read(string,*)nsam(i)
	   read(lu+i,101)string
	   read(string,*)isum
	   sumti = sumti + isum
	   read(lu+i,101)string
	   read(string,*)jsum
	   sumtj = sumtj + jsum
  	   if(debug)write(*,*)' nsam ',nsam(i),
     &              ' isum ',isum,' jsum ',jsum,' nf ',nf
	   do j=1,3
              read(lu+i,*)
           end do

	   bval = dble(nsam(i))
	   tval = tval + bval

	   if(lmarg1)then
 	   if(debug)write(*,*)' reading 1D marginals '
	   do j=1,nd+nsp
	      read(lu+i,*)
	      read(lu+i,*)
	      read(lu+i,*)
	      do k=1,ndis1
	         read(lu+i,*)axis(k,j),aval
	         marginal1D(k,j) = marginal1D(k,j) + aval*bval
              end do
           end do
	   end if

	   if(lmarg2)then
 	   if(debug)write(*,*)' reading 2D marginals '
	   do j=1,n2d
	      read(lu+i,*)
	      read(lu+i,102)string
	      read(string,*)nmarg(1,j),nmarg(2,j)
	      read(lu+i,*)
	      read(lu+i,*)range2d(1,j),range2d(2,j),
     &                    range2d(3,j),range2d(4,j)
	      read(lu+i,*)
	      if(debug)write(*,*)' variables ',nmarg(1,j),nmarg(2,j)
	     do k=1,ndis2
		    read(lu+i,*)(m2d(kk,k),kk=1,ndis2)
	     do kk=1,ndis2
	        marginal2D(kk,k,j) = marginal2D(kk,k,j) + bval*m2d(kk,k)
         end do
         end do
       end do
	   end if

	   if(lcov)then
	   read(lu+i,*)
	   read(lu+i,*)
	   read(lu+i,*)
	   do j=1,nd+nsp
	      read(lu+i,*)
	      read(lu+i,*)(covar(k,j),k=1,nd+nsp)
	   end do

	   read(lu+i,*)
	   read(lu+i,*)
	   read(lu+i,*)
	   do j=1,nd+nsp
	      read(lu+i,*)
	      read(lu+i,*)(ecovar(k,j),k=1,nd+nsp)
	   end do

c						note that there may be a
c						problem with
c						formulae for combining
c						independent runs 
c						because of potential for zeros
c						in ecov array.
c						(could replace with simple
c						weigthed averages ?) 
c						
 	   do j=1,nd+nsp
 	      do k=1,nd+nsp
                 e = ecovar(k,j)
                 ee = e*e
                 if(e.eq.0.0)then
 		 else
 	           tcov(k,j)  = tcov(k,j) + covar(k,j)/ee
 	           tecov(k,j) = tecov(k,j) + 1./ee
                  end if
 	      end do
 	   end do

c	   do j=1,nd+nsp
c	      do k=1,nd+nsp
c	           tcov(k,j)  = tcov(k,j) + covar(k,j)
c	           tecov(k,j) = tecov(k,j) + ecovar(k,j)
c	      end do
c	   end do
	   end if

c						read in PSR data
  15       read(lu+i,100,end=30)string
	   if(string(3:11).ne.'PSR data:')go to 15
	   do k=1,7
	      read(lu+i,*)
	   end do
           kk2 = kk1+nw(i)-1
	   if(debug)write(*,*)' i = ',i
	   if(debug)write(*,*)' nw(i) = ',nw(i)
	   if(debug)write(*,*)' kk1 = ',kk1
	   if(debug)write(*,*)' kk2 = ',kk2
	   if(kk2.gt.nwmax)then
	      write(*,*)
	      write(*,*)' Error - maximum number of walks is too small'
	      write(*,*)'         Value of parameter nwmax = ',nwmax
	      write(*,*)'         Must be at least         = ',kk2
	      write(*,*)
	      write(*,*)' Remedy - increase size of nwmax and recompile'
	      write(*,*)
	      stop
	   end if
	   read(lu+i,*)(psr_sum(k),k=kk1,kk2)
	   do j=1,nd+nsp
	      read(lu+i,*)
     &        (psr_mean(j,k),psr_cov(j,k),k=kk1,kk2)
	   end do
           kk1 = kk1+nw(i)
	   nwt = kk2

  16       read(lu+i,100,end=30)string
	   if(string(13:23).ne.'Expectation')go to 16
	   
	   read(lu+i,*)
	   read(lu+i,*)
	   do j=1,nd+nsp
              read(lu+i,103)string
              read(string,*)sp,esp
              if(esp.gt.0)then
                esp = esp*esp
                sp_int(j) = sp_int(j)  + sp/esp
                esp_int(j) = esp_int(j) + 1./esp 
	      end if
	   end do

 30     continue

	end do

	if(lcov)then
c						combine covariances and errors 
 	do j=1,nd+nsp
 	   do k=1,nd+nsp
               e = tecov(k,j)
               if(e.eq.0.0)then
 	         tecov(k,j) = 0.0
 	      else
 	         tcov(k,j)  = tcov(k,j)/e
 	         tecov(k,j) = 1./sqrt(e)
              end if
 	   end do
 	end do
	end if

c						simple average is an alternative
	a1 = real(nf)
c	if(lcov)then
c	do j=1,nd+nsp
c	   do k=1,nd+nsp
c	        tcov(k,j)  = tcov(k,j)/a1
c	        tecov(k,j) = tecov(k,j)/a1 
c	   end do
c	end do
c	end if

	do j=1,nd+nsp
	   if(esp_int(j).ne.0)then
              sp_int(j) = sp_int(j)/esp_int(j)
              esp_int(j) = 1./sqrt(esp_int(j))
	   end if
	end do
c
c						calculate combined 
c						PSR-factor
c
	do j=1,nd+nsp
	   psr1 = 0.0
	   bvar = 0.0
           do k=1,nwt
              psr1 = psr1 + psr_mean(j,k)
	   end do
           psr1 = psr1/real(nwt)

           do k = 1,nwt
              a = psr_mean(j,k) - psr1
              a = a*a
              bvar = bvar + a
           end do
           bvar = bvar/(nwt-1)

           dou = 0.
           do k = 1,nwt
              b = psr_mean(j,k)
              dou = dou + psr_cov(j,k)/psr_sum(k) - b*b
           end do
           dou = dou/real(nwt)

	   psr(j) = 1.0 + bvar/dou
           psr(j) = sqrt(psr(j))

	end do
c						normalize 1D marginals
	if(lmarg1)then
           do j=1,nd+nsp
              do k=1,ndis1
                 marginal1D(k,j) = marginal1D(k,j)/tval
              end do
           end do
	end if

c						normalize 2D marginals
	if(lmarg2)then
           do j=1,n2d
              do k=1,ndis2
                 do kk=1,ndis2
                    marginal2D(kk,k,j) = marginal2D(kk,k,j)/tval
                 end do
              end do
           end do
	end if

	sumti = sumti/tval
	sumtj = sumtj/tval

 	call writeout(axis,sp_int,esp_int,tcov,tecov,
     &                nmarg,marginal1D,marginal2D,nsam,nw,nf,
     &                ne,nd,nstp,icst,nres,n2d,nsp,ndis1,ndis2,
     &                rangep,range2d,sf,iseed,sumti,sumtj,
     &                psr,psr_mean,psr_cov,psr_sum,
     &                quasi,umc,lmarg1,lmarg2,lcov,lspec)

 100    format(a72)
 101    format(57x,a72)
 102    format(31x,a72)
 103    format(5x,a72)
 104    format(24x,a72)

	stop
	end

	subroutine zero_rarray(a,n)
	real*4	a(n)
	do i=1,n
	   a(i) = 0.0
        end do
	return
	end

	subroutine zero_darray(a,n)
	real*8	a(n)
	do i=1,n
	   a(i) = 0.d0
        end do
	return
	end

	subroutine zero_iarray(j,n)
	integer	j(n)
	do i=1,n
	   j(i) = 0
        end do
	return
	end

 	subroutine writeout
     &             (axis,sp_int,esp_int,tcov,tecov,
     &             nmarg,marginal1D,marginal2D,nsam,nw,nf,
     &             ne,nd,nstp,icst,nres,n2d,nsp,ndis1,ndis2,
     &             rangep,range2d,sf,
     &             iseed,sumti,sumtj,
     &             psr,psr_mean,psr_cov,psr_sum,
     &             quasi,umc,lmarg1,lmarg2,lcov,lspec)

	include		'nab_param.inc'

	parameter	(maxfiles=100)

        real*4          tcov(ndmax+nspmax,ndmax+nspmax)
        real*4          tecov(ndmax+nspmax,ndmax+nspmax)
        real*4          sp_int(ndmax+nspmax)
        real*4          esp_int(ndmax+nspmax)
        real*4          psr(ndmax+nspmax)
        real*4          psr_mean(ndmax+nspmax,nwmax)
        real*4          psr_cov(ndmax+nspmax,nwmax)
        real*4          psr_sum(nwmax)
        real*4          axis(ndismax,ndmax+nspmax)
        real*4          range2d(4,nmarg2Dmax)
        real*4          rangep(4,ndmax+nspmax)
        real*4          sf(ndmax+nspmax)

        real*8          marginal1D(ndismax,ndmax+nspmax)
        real*8          marginal2D(ndismax,ndismax,nmarg2Dmax)

        integer         nw(maxfiles)
        integer         nsam(maxfiles)
        integer         nmarg(2,nmarg2Dmax)

	logical		lmarg1,lmarg2,lspec,lcov
	logical		quasi
	logical		umc

	lu_out = 6

	ns = 0
	nr = 0
	do i=1,nf
          ns = ns + nsam(i)
          nr = nr + nw(i)
        end do

        write(lu_out,100)
	if(umc)then
           write(lu_out,
     &     fmt='(/"  Program nab - Monte Carlo integration",
     &     " of"/17x,"Bayesian integrals using uniform sampling"/)')
	else
           write(lu_out,
     &     fmt='(/"  Program nab - Monte Carlo integration",
     &     " of Bayesian integrals"/
     &     16x,"using Neighbourhood importance sampling"/)')
	end if
        write(lu_out,100)
        write(lu_out,fmt='(2x,"Summary of input options"/)')
        write(lu_out,*)' Number of models in ensemble   : ',ne
        write(lu_out,*)' Number of dimensions           : ',nd
        write(lu_out,*)' Number of random walks         : ',nr
        write(lu_out,*)' Number of steps in first walk  : ',nstp
        write(lu_out,*)' Total number of samples        : ',ns
        write(lu_out,*)' Rate of refresh in dlist (1st) : ',nres
        write(lu_out,*)' Starting cell for first walk   : ',icst
        if(quasi)
     &  write(lu_out,*)' Type of random deviate : Quasi  : seed ',iseed
        if(.not.quasi)
     &  write(lu_out,*)' Type of random deviate : Pseudo : seed ',iseed

        write(lu_out,*)
        write(lu_out,*)' MC integration options: Combined results ',
     &  'from ',nf,' independent runs of program nab'
        write(lu_out,*)
	if(lmarg1)then
           write(lu_out,*)
     &     ' Calculate 1D marginal pdfs                  : yes'
	else
           write(lu_out,*)
     &     ' Calculate 1D marginal pdfs                  : no'
	end if
	if(lcov)then
           write(lu_out,*)
     &     ' Covariance matrix                           : yes'
	else
           write(lu_out,*)
     &     ' Covariance matrix                           : no'
	end if
        write(lu_out,*)
     &  ' Number of 2D marginal pdfs to be calculated : ',n2d
        write(lu_out,*)
     &  ' Number of user supplied special functions   : ',nsp
        write(lu_out,*)
     &  ' Number of bins per axis for 1D marginals    : ',ndis1
        write(lu_out,*)
     &  ' Number of bins per axis for 2D marginals    : ',ndis2
        write(lu_out,*)
     &  ' Output frequency of numerical integrals     : Final only'
        write(lu_out,100)

c						write out parameter 
c						space details from first file
c
        write(lu_out,fmt=
     &  '("  Parameter space details :"/)')
        write(lu_out,*)' Number of dimensions           : ',nd
        write(lu_out,*)' '
        write(lu_out,*)' Parameter ranges'
        write(lu_out,*)'   Number   Minimum     Maximum   ',
     &                 ' Scale factor Scaled min  Scaled max'

	do i=1,nd
           write(lu_out,101)i,rangep(1,i),rangep(2,i),sf(i),
     &                      rangep(3,i),rangep(4,i)
	end do
        if(nsp.gt.0)then
           write(lu_out,*)
           write(lu_out,*)' Ranges of special functions'
           write(lu_out,*)
           write(lu_out,*)'                Min         Max'
	   do j=nd+1,nd+nsp
           write(lu_out,102)j-nd,rangep(1,j),rangep(2,j),j
	   end do
        end if
c
c						write out header for 
c						final results

        write(lu_out,100)
        if(umc)then
        write(lu_out,*)' Results of Monte Carlo ',
     &              'integration using uniform sampling'
        write(lu_out,*)
        write(lu_out,*)
     &  ' Number of uniform random samples generated         : ',ns
        write(lu_out,*)' Average number of Voronoi cells',
     &                 ' sampled per walk     : ',sumtj
        else
        write(lu_out,*)' Results of Monte Carlo ',
     &              'integration using NA random walk'
        write(lu_out,*)
        write(lu_out,*)
     &  ' Number of random samples generated by NA-walks       : ',ns
        write(lu_out,*)' Average number of Voronoi cells',
     &                 ' intersected by axes  :',sumti
        write(lu_out,*)' Average number of Voronoi cells',
     &                 ' sampled per NA-walk  : ',sumtj
        end if
	write(lu_out,100)

        if(lmarg1)then

           do i=1,nd
              write(lu_out,*)
              write(lu_out,*)' Marginal for parameter :',i
              write(lu_out,*)
              do j=1,ndis1
                 write(lu_out,*)axis(j,i),marginal1D(j,i)
              end do
           end do
           do i=1,nsp
              write(lu_out,*)
              write(lu_out,*)' Marginal for special parameter :',i
              write(lu_out,*)
              do j=1,ndis1
                 write(lu_out,*)axis(j,i+nd),marginal1D(j,i+nd)
              end do
           end do

        end if
c                                               write out 2D marginals
        if(lmarg2)then

           do i=1,n2d
              k1 = nmarg(1,i)
              k2 = nmarg(2,i)
              write(lu_out,*)
              write(lu_out,*)' 2D Marginal for parameters :',k1,k2
              write(lu_out,*)'    Parameter ranges : '
              write(lu_out,*)range2d(1,i),range2d(2,i),
     &                       range2d(3,i),range2d(4,i)
              write(lu_out,*)
              do jj = 1,ndis2
                 write(lu_out,*)(marginal2D(ii,jj,i),ii=1,ndis2)
              end do
           end do

        end if

        if(lcov)then
           write(lu_out,*)
           write(lu_out,*)' Covariance matrix :'
           write(lu_out,*)
           do i=1,nd+nsp
              write(lu_out,*)i,' :'
              write(lu_out,103)(tcov(j,i),j=1,nd+nsp)
           end do
           write(lu_out,*)
           write(lu_out,*)' Numerical error in covariance matrix :'
           write(lu_out,*)
           do i=1,nd+nsp
              write(lu_out,*)i,' :'
              write(lu_out,103)(tecov(j,i),j=1,nd+nsp)
           end do
        end if

c       if(lspec)then
c          write(lu_out,*)
c          write(lu_out,*)' special function integration :'
c          write(lu_out,*)
c          write(lu_out,*)'  I      Integral     Error'
c          do i=1,nsp
c             write(lu_out,*)i,' : ',sp_int(i),esp_int(i)
c          end do
c          write(lu_out,*)
c       end if

c							write out 
c							PSR data 
c
c							write out 
c							expectation values

        write(lu_out,400)nr,nd+nsp
        write(lu_out,*)(psr_sum(j),j=1,nr)
	do i=1,nd+nsp
           write(lu_out,*)
     &     (psr_mean(i,j),psr_cov(i,j),j=1,nr)
        end do
        write(lu_out,100)
c
c       write(lu_out,*)
        write(lu_out,*)'           Expectation values',
     &        '                    Values for a uniform prior'
	if(umc)then
           write(lu_out,*)'  P        Mean       Stn err  ',
     &           '   Num err      Mean         Stn err'
	else
           write(lu_out,*)'  P        Mean       Stn err  ',
     &           '   Num err   PSR-stat  Mean         Stn err'
	end if
        write(lu_out,*)
        do i=1,nd+nsp
           b  = sp_int(i)
           b3 = esp_int(i)
           b2 = esp_int(i)/sqrt(real(ns))
           av = rangep(1,i)+rangep(2,i)
           bv = rangep(2,i)-rangep(1,i)
           um  = 0.5*av
           usd = sqrt(bv*bv/12.)

           if(.not.umc)then
              write(lu_out,
     &        fmt='(i4,":",3(F12.6,1x),f6.3,1x,2(F12.6,1x))')
     &        i,b,b3,b2,psr(i),um,usd
           else
              write(lu_out,
     &        fmt='(i4,":",5(F12.6,1x))')
     &        i,b,b3,b2,um,usd
           end if
        end do
        write(lu_out,100)

   99   format(72("-"))
  100   format(/,72("-")/)
  101   format(3x,i4,2x,5(f11.4,1x))
  102   format(3x,i4,3x,2(f11.4,1x),3x,i4)
  103   format(5(E12.5,2x))
  400   format(1x,72("-")/
     &  '  PSR data:'/
     &  11x,'The following data is used in calculation of the '/
     &  11x,'potential scale reduction statistic (PSR). '/
     &  11x,'This is needed to calculate the overall PSR when'/
     &  11x,'combining the multiple runs of nab'/
     &  11x,'(The utility program called combine does this.) '/
     &     1x,72("-")/1x,i9,2x,i9)


	return
	end

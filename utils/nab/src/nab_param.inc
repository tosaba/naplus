c
c----------------------------------------------------------------------
c
c	Parameter file for program nab
c
c       Parameters:
c
c                                Internal working arrays
c
c               nemax          - maximum number of models in input ensemble
c               ndmax          - maximum number of dimensions
c               nhmax          - maximum size (in bytes) of nad file header
c               nwmax          - maximum number of independent random walks
c               nint_max       - maximum number of Voronoi cells intersected
c                                along an axis (<=nemax)
c
c				 Output data arrays
c
c               ndismax        - maximum number of intervals for each axis
c                                used to represent marginal pdfs. (~50)
c               nmarg2Dmax     - maximum number of 2D marginal pdfs.
c               nspmax         - maximum number of special functions
c                                to be integrated
c		maxseq	       - maximum number of quasi random sequences.
c				 (should be >= ndmax+1)
c
c                                       M. Sambridge, RSES, March 1998
c
c----------------------------------------------------------------------
c
c
        parameter       (nemax=500000)
        parameter       (ndmax=30)
        parameter       (nhmax=5000)
        parameter       (nwmax=8001)
        parameter       (nint_max=1000)
        parameter       (ndismax=500)
        parameter       (nmarg2Dmax=200)
        parameter       (nspmax=70)
        parameter       (maxseq=500)


function [sh,cbh] = plot_NAmodelssingle(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   plot_NAmodels(result,log_threshold)
%   - this function uses the struct format created by 
%     result = read_NAD('filename')
%
%   Tobias Bauman, ETH Zuerich, June 2011
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 1
    result          = varargin{1};
    log_threshold   = max(log10(result.misfit));
    names           = [];
    style           = 'gather';
    split           = -0.5;
    mcaxis          = [];
end
if nargin == 2
    result          = varargin{1};
    log_threshold   = varargin{2};
    names           = [];
    style           = 'gather';
    split           = -0.5;
    mcaxis          = [];
end
if nargin == 3
    result          = varargin{1};
    log_threshold   = varargin{2};
    names           = varargin{3};
    if length(names) ~= result.numofvar
        error(['check the number of parameter names \n' ...
               'there are ' num2str(result.numofvar) 'parameters']);
    end   
    style           = 'gather';
    split           = -0.5;
    mcaxis          = [];
end
if nargin == 4
    result          = varargin{1};
    log_threshold   = varargin{2};
    names           = varargin{3};
    if length(names) ~= result.numofvar
        error(['check the number of parameter names \n' ...
               'there are ' num2str(result.numofvar) 'parameters']);
    end   
    style           = varargin{4};
    split           = -0.5; 
    mcaxis          = [];
end
if nargin == 5
    result          = varargin{1};
    log_threshold   = varargin{2};
    names           = varargin{3};
    if length(names) ~= result.numofvar
        error(['check the number of parameter names \n' ...
               'there are ' num2str(result.numofvar) 'parameters']);
    end   
    style           = varargin{4};
    split           = varargin{5};
    mcaxis          = [];
end
if nargin == 6
    result          = varargin{1};
    log_threshold   = varargin{2};
    names           = varargin{3};
    if length(names) ~= result.numofvar
        error(['check the number of parameter names \n' ...
               'there are ' num2str(result.numofvar) 'parameters']);
    end   
    style           = varargin{4};
    split           = varargin{5};
    mcaxis          = varargin{6};
end
if nargin > 6
    error('there are too many arguments')
end


% split 


% get a numbering scheme for the subplots
numbering       = zeros(result.numofvar);
numbering(:)    = 1:(result.numofvar*result.numofvar);

% threshold
ind1 = find(log10(result.misfit) <= log_threshold);
result.parameter    = result.parameter(ind1,:);
result.misfit       = result.misfit(ind1);
result.key          = result.key(ind1);

% sort the data to plot the lowest misfit data on top
[~,ix] = sort(result.misfit);

% plotting routine
if ~strcmp(style,'separated')
    figure('name',['filename: ' result.filename]);
    %[sh,cbh]=plotting_routine(result,numbering,ix,names,style);
    [sh,cbh]=plotting_routine(result,numbering,ix,names,split,mcaxis);

else
    
    k  = [5];
    kk = [6];
    
    %k  = [4 5 4];
    %kk = [5 6 6];
    
    for n = 1:length(k)
        figure('name',['filename: ' result.filename]);
        plotsingle(result,ix,names,k(n),kk(n),split);
        
    end
        
        

            
end
    
    
end
% END OF FUNCTION =========================================================

function plotsingle(result,ind,names,k,kk,split)
    if isfield(result,'true')
        hold on
        plot([result.paramLB(k) result.paramRB(k)],result.true(kk)*[1 1],'r')
        plot(result.true(k)*[1 1],[result.paramLB(kk) result.paramRB(kk)],'r')
    end

    scatter(result.parameter(ind,k),result.parameter(ind,kk),7,log10(result.misfit(ind)),'filled');
    axis square

    xlim([result.paramLB(k) result.paramRB(k)])
    ylim([result.paramLB(kk) result.paramRB(kk)])
    caxis([floor(min(log10(result.misfit))) ceil(max(log10(result.misfit)))])
    cmap = custom_colormap(gca,split,32);
    colormap(cmap)
    cb = colorbar;
    
    
    if isempty(names)
        hxl = xlabel(['P # ' num2str(k)]);
        hyl = ylabel(['P # ' num2str(kk)]);
        hcl = ylabel(cb,'log_{10}(misfit)');
    else
        hxl = xlabel(names(k));
        hyl = ylabel(names(kk));
        hcl = ylabel(cb,'log_{10}(misfit)');
    end
        
    
    if isfield(result,'true')
        hold on
        plot_truemodel(result,k,kk)
    end
    
    
    custom_nicefonts(hxl,'axis')
    custom_nicefonts(hyl,'axis')
    custom_nicefonts(hcl,'axis')
    custom_nicefonts(gca,'plot')
    custom_nicefonts(cb,'colorbar')
    
end
% plotting subroutine 
function [sh,cb] = plotting_routine(result,numbering,ind,names,split,mcaxis)
    
n=1;
for k = 1:result.numofvar
    for kk = 1:result.numofvar
        if k ~= kk
            sh(n)= mysubplot(result.numofvar,result.numofvar,numbering(k,kk));
            scatter(result.parameter(ind,k),result.parameter(ind,kk),5,log10(result.misfit(ind)),'filled');
            %scatter(result.parameter(ind(1:300),k),result.parameter(ind(1:300),kk),5,log10(result.misfit(ind(1:300))),'filled');
            if isfield(result,'true')
                hold on
                plot_truemodel(result,k,kk)
            end
            
            
            %colormap(coupled_colormap(log10(result.misfit),0));
            %set(gca,'Clim',[floor(min(log10(result.misfit))) ceil(max(log10(result.misfit)))]);
            
            if (~isempty(mcaxis))
                caxis([mcaxis(1) mcaxis(2)]);
            else
                caxis([floor(min(log10(result.misfit))) ceil(max(log10(result.misfit)))])
            end
            
            cmap = custom_colormap(gca,split,32);
            colormap(cmap)
            
            axis square
            xlim([result.paramLB(k) result.paramRB(k)])
            ylim([result.paramLB(kk) result.paramRB(kk)])
            set_nicefonts(gca,1);

            
        else
            sh(n) = mysubplot(result.numofvar,result.numofvar,numbering(k,kk));
            hist(result.parameter(:,k))
            %edges = linspace(result.paramLB(k),result.paramRB(k),101);
            %x_edges = (edges(2:end)-edges(1:end-1))/2 + edges(1:end-1);
            %y_edges  = histc(result.parameter(:,k),edges);
            %plot(x_edges,y_edges(1:end-1),'r');
            xlim([result.paramLB(k) result.paramRB(k)])
            %ylim([0 700])
            ylim([0 result.numofmod])
            axis square
            set_nicefonts(gca,2);
        end
        if k~=1 
            set(gca,'YTicklabel',[]);
        else
            if kk ~= 1
                if isempty(names)
                    hyl =ylabel(['P # ' num2str(kk)]);
                    set(hyl ,'FontSize', 10);
                else
                    hyl =ylabel(names(kk));
                    set(hyl ,'FontSize', 10);
                end
            else
                    hyl =ylabel('#  of models');
                    set(hyl ,'FontSize', 10);                
            end
        end
        
        if k == result.numofvar
            if kk~= result.numofvar
                if isempty(names)
                    hyl =ylabel(['P # ' num2str(kk)]);
                    set(hyl ,'FontSize', 10);
                    set(gca,'yaxislocation','right');
                else
                    hyl =ylabel(names(kk));
                    set(hyl ,'FontSize', 10);
                    set(gca,'yaxislocation','right');
                end
            else
                    hyl =ylabel('# of models');
                    set(hyl ,'FontSize', 10);
                    set(gca,'yaxislocation','right');
            end
        end
        
        if kk~= result.numofvar
            set(gca,'XTicklabel',[]);
        else
            if isempty(names)
                hxl = xlabel(['P # ' num2str(k)]);
                set(hxl ,'FontSize', 10);
            else
                hxl = xlabel(names(k));
                set(hxl ,'FontSize', 10);
            end

        end
        
        % title with numerical issues
        nstr = length(num2str(result.numofmod));
                
%         if kk == 1
%                if k == 1 
%                     title({[num2str_aligned(result.numofvar,nstr,'blanks') '  : # of variables'],[num2str_aligned(result.numofmod,nstr,'blanks') '  : # of models in ensemble'],'','',''},'Units', 'normalized','Position', [0 1], 'HorizontalAlignment', 'left','FontName','AvantGarde','FontSize',7);
%                end
%                if k == 3
%                    %title({result.header{[7 8]},'','',''},'Units', 'normalized','Position', [0 1], 'HorizontalAlignment', 'left','FontName','AvantGarde','FontSize',7);
%                         title({ [num2str_aligned(result.numofruns,nstr,'blanks') ' : # of iterations'],...
%                                 [num2str_aligned(result.numof1stit,nstr,'blanks') ' : # of samples at iteration 1'],'','',''},...
%                                 'Units', 'normalized','Position', [0 1], 'HorizontalAlignment', 'left','FontName','AvantGarde','FontSize',7);
%                end
%                if k == 5
%                    %title({result.header{9} ,'','',''},'Units', 'normalized','Position', [0 1], 'HorizontalAlignment', 'left','FontName','AvantGarde','FontSize',7);
%                         title({[num2str_aligned(result.numofsampsperit,nstr,'blanks') ' : # of samples at other iterations'],...
%                                [num2str_aligned(result.numofcells2resamp,nstr,'blanks') ' : # of cells to resample'],'','',''},...
%                                'Units', 'normalized','Position', [0 1], 'HorizontalAlignment', 'left','FontName','AvantGarde','FontSize',7);
% 
%                end
%         end     
        
        n = n+1;
    end
end





    % get a colorbar for the entire figure
    
    % get postions
    for k = 1:(result.numofvar*result.numofvar)
        pos{k}=get(sh(k), 'Position');
    end
    
    cb = colorbar('peer',sh(end-1));
    
    % adjust the scaling
    set(cb,'YTick',[floor(min(log10(result.misfit))):ceil(max(log10(result.misfit)))]);
    set(cb,'TickDir','out')
    %set(cb,'YminorTick','on')
    
    % reset subplot positions
    % position: [left bottom width height]
    set(cb, 'Position', [.88 .11 .03 .8])
    cblabel = ylabel(cb,'log_{10}(\chi^2)');
    
    set(cblabel,'FontName', 'AvantGarde');
    for k=1:(result.numofvar*result.numofvar)
        set(sh(k), 'Position', [0.95* pos{k}(1) pos{k}(2) pos{k}(3) 0.9*pos{k}(4)]);
    end
    set_nicefonts(cb,3);
    
    % colorbar
    % second axis because of its box
    ax1 = axes ('Position', get (cb, 'Position'));
    % turn cbh box off ...
    set(cb,'Box','off') 
    % ... and turn ax1 box on
    set(ax1,'Box','on','Color','none','XTicklabel',[],'YTicklabel',[],'xtick',[],'ylim',get(cb,'ylim'),'ytick',get(cb,'ytick'),'Tickdir','in')
    set_nicefonts(ax1,3);
end


function set_nicefonts(gca,num_case)

if num_case == 1
            set( gca                       , ...
            'FontName'   , 'AvantGarde' , ...
            'FontSize'   , 8           );

        set(gca, ...
          'Box'         , 'on'     , ...
          'TickDir'     , 'out'     , ...
          'TickLength'  , [.03 .03] , ...
          'XMinorTick'  , 'on'      , ...
          'YMinorTick'  , 'on'      , ...
          'YGrid'       , 'on'      , ...
          'XGrid'       , 'on'      , ...
          'XColor'      , [.3 .3 .3], ...
          'YColor'      , [.3 .3 .3], ...
          'LineWidth'   , 0.7         );
end
if num_case ==2
                set( gca                       , ...
            'FontName'   , 'AvantGarde' , ...
            'FontSize'   , 8           );

        set(gca, ...
          'Box'         , 'on'     , ...
          'TickDir'     , 'out'     , ...
          'TickLength'  , [.03 .03] , ...
          'XMinorTick'  , 'on'      , ...
          'YMinorTick'  , 'on'      , ...
          'XColor'      , [.3 .3 .3], ...
          'YColor'      , [.3 .3 .3], ...
          'LineWidth'   , 0.7         );
end

if num_case ==3
                set( gca                       , ...
            'FontName'   , 'AvantGarde' );
        set(gca, ...
          'LineWidth'   , 0.7         );
end
end

function plot_truemodel(result,k,kk)
    plot(result.true(k),result.true(kk),'o','markersize',15,'linewidth',1,'Markeredgecolor','r');%[0.7 0.7 0.7]);
    plot(result.true(k),result.true(kk),'+','markersize',5,'linewidth',1,'Markeredgecolor','r');%[0.7 0.7 0.7]);
end
